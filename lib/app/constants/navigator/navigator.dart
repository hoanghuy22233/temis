import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:temis/presentation/router.dart';

class AppNavigator {
  AppNavigator._();

  static navigateBack() async {
    Get.back();
  }

  static navigatePopUtil({String name}) async {
    Navigator.popUntil(Get.context, ModalRoute.withName(name));
  }

  static navigateSplash() async {
    var result = await Get.toNamed(BaseRouter.SPLASH);
    return result;
  }

  static navigateLogin() async {
    var result = await Get.offAllNamed(BaseRouter.LOGIN);
    return result;
  }

  static navigateNavigation() async {
    var result = await Get.offAllNamed(BaseRouter.NAVIGATION);
    return result;
  }

  static navigateSlideShow() async {
    var result = await Get.offAllNamed(BaseRouter.SLIDESHOW);
    return result;
  }

  static navigateMenu() async {
    var result = await Get.offAllNamed(BaseRouter.MENU);
    return result;
  }

  static navigateRegister() async {
    var result = await Get.toNamed(BaseRouter.REGISTER);
    return result;
  }

  static navigateForgotPass() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASS);
    return result;
  }

  static navigateForgotPassVerifi() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASS_VERIFY);
    return result;
  }

  static navigateForgotPassReset() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASS_RESET);
    return result;
  }

  static navigateRegisterVerifi() async {
    var result = await Get.toNamed(BaseRouter.REGISTER_VERIFY);
    return result;
  }

  static navigateFoodService() async {
    var result = await Get.toNamed(BaseRouter.FOOD_SERVICE);
    return result;
  }

  static navigateWorkService() async {
    var result = await Get.toNamed(BaseRouter.WORK_NAVIGATION);
    return result;
  }

  static navigateNavigationCafe({int id}) async {
    var result = await Get.toNamed(BaseRouter.NAVIGATION_CAFE, arguments: {
      'idStore': id,
    });
    return result;
  }

  static navigateNavigationFastFood() async {
    var result = await Get.toNamed(BaseRouter.NAVIGATION_FOOD);
    return result;
  }
}
