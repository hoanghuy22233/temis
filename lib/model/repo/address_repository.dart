import 'package:temis/model/api/request/barrel_request.dart';
import 'package:temis/model/api/response/barrel_response.dart';
import 'package:temis/model/api/rest_client.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class AddressRepository {
  final Dio dio;

  AddressRepository({@required this.dio});

}
