class BannerItem {
  final String image, banner, id;
  BannerItem({this.image, this.id, this.banner});
}

List<BannerItem> itemBanner = [
  BannerItem(
      image: "assets/images/web_verical.jpg",
      banner: "assets/images/register_bg.png"),
  BannerItem(
      image: "assets/images/app.jpg", banner: "assets/images/register_bg.png"),
  BannerItem(
      image: "assets/images/service_verical.jpg",
      banner: "assets/images/register_bg.png"),
  BannerItem(
      image: "assets/images/cntt_verical.jpg",
      banner: "assets/images/register_bg.png"),
  BannerItem(
      image: "assets/images/game_verical.jpg",
      banner: "assets/images/register_bg.png"),
  BannerItem(
      image: "assets/images/vps_verical.jpg",
      banner: "assets/images/register_bg.png"),
];
