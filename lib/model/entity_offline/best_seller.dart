class BestSeller {
  final String image, name;
  final int idStore;
  final double price, priceSale;
  BestSeller( {this.idStore, this.image, this.name, this.price, this.priceSale});
}

List<BestSeller> itemBestSeller = [
  BestSeller(
      name: "Bánh tráng trộn",
      image: "assets/images/ricepaper.jpg",
      idStore: 13,
      price: 21000,
      priceSale: 15000
  ),
  BestSeller(
      name: "Bánh tráng cuộn",
      image: "assets/images/ricepaper.jpg",
      idStore: 13,
      price: 21000,
      priceSale: 15000
  ),
  BestSeller(
      name: "Bánh tráng nướng",
      image: "assets/images/ricepaper.jpg",
      idStore: 13,
      price: 21000,
      priceSale: 15000
  ),
  BestSeller(
      name: "Trứng cút xào me cốt dừa",
      image: "assets/images/ricepaper.jpg",
      idStore: 13,
      price: 21000,
      priceSale: 15000
  ),
  BestSeller(
      name: "Combo bánh tráng 1 trộn + 1 cuộn",
      image: "assets/images/ricepaper.jpg",
      idStore: 13,
      price: 38000,
      priceSale: 30000
  ),

  BestSeller(
      name: "Capuchino",
      image: "assets/images/coffee.jpg",
      idStore: 11,
      price: 30000,
      priceSale: 20000
  ),
  BestSeller(
      name: "Đen đá",
      image: "assets/images/coffee.jpg",
      idStore: 11,
      priceSale: 20000
  ),
];
