class CoffeeItem {
  final String image, name;
  final int id, idStore;
  final double price, star;
  CoffeeItem({this.name,this.image, this.id,  this.idStore, this.price, this.star});
}

List<CoffeeItem> coffeeItem = [
  CoffeeItem(
      name: "Cà phê đen nóng",
      id: 1,
      idStore: 11,
      image: "assets/images/coffee_item1.jpg",
      price: 30000,
      star: 5
  ),
  CoffeeItem(
      name: "Cà phê sữa nghệ thuật",
      id: 2,
      idStore: 11,
      image: "assets/images/coffee_item2.jpg",
      price: 35000,
      star: 4.7
  ),
  CoffeeItem(
      name: "Cà phê sữa mặt cười",
      id: 3,
      idStore: 11,
      image: "assets/images/coffee_item3.jpg",
      price: 38000,
      star: 5.0
  ),
  CoffeeItem(
      name: "Cà phê đánh bọt",
      id: 4,
      idStore: 11,
      image: "assets/images/coffee_item4.jpg",
      price: 40000,
      star: 4.8
  ),
  CoffeeItem(
      name: "Cà phê sữa đá",
      id: 5,
      idStore: 11,
      image: "assets/images/coffee_item5.jpg",
      price: 25000
  ),
  CoffeeItem(
      name: "Cà phê đen nóng",
      id: 6,
      idStore: 11,
      image: "assets/images/coffee_item1.jpg",
      price: 30000,
      star: 5
  ),
  CoffeeItem(
      name: "Cà phê sữa nghệ thuật",
      id: 7,
      idStore: 11,
      image: "assets/images/coffee_item2.jpg",
      price: 35000,
      star: 4.7
  ),
  CoffeeItem(
      name: "Cà phê sữa mặt cười",
      id: 8,
      idStore: 11,
      image: "assets/images/coffee_item3.jpg",
      price: 38000,
      star: 5.0
  ),
  CoffeeItem(
      name: "Cà phê đánh bọt",
      id: 9,
      idStore: 11,
      image: "assets/images/coffee_item4.jpg",
      price: 40000,
      star: 4.8
  ),
  CoffeeItem(
      name: "Cà phê sữa đá",
      id: 10,
      idStore: 11,
      image: "assets/images/coffee_item5.jpg",
      price: 25000
  ),
  CoffeeItem(
      name: "Cà phê đen nóng",
      id: 11,
      idStore: 11,
      image: "assets/images/coffee_item1.jpg",
      price: 30000,
      star: 5
  ),
  CoffeeItem(
      name: "Cà phê sữa nghệ thuật",
      id: 12,
      idStore: 11,
      image: "assets/images/coffee_item2.jpg",
      price: 35000,
      star: 4.7
  ),
  CoffeeItem(
      name: "Cà phê sữa mặt cười",
      id: 13,
      idStore: 11,
      image: "assets/images/coffee_item3.jpg",
      price: 38000,
      star: 5.0
  ),
  CoffeeItem(
      name: "Cà phê đánh bọt",
      id: 14,
      idStore: 11,
      image: "assets/images/coffee_item4.jpg",
      price: 40000,
      star: 4.8
  ),
  CoffeeItem(
      name: "Cà phê sữa đá",
      id: 15,
      idStore: 11,
      image: "assets/images/coffee_item5.jpg",
      price: 25000
  ),
];
