class ListFastFoodItem {
  final int id, like, read;
  final String name, image, price, salePrice, content, date;
  ListFastFoodItem(
      {this.like,
      this.id,
      this.name,
      this.image,
      this.price,
      this.salePrice,
      this.content,
      this.date,
      this.read});
}

List<ListFastFoodItem> listFastFood = [
  ListFastFoodItem(
      read: 0,
      like: 0,
      price: "12000 đ",
      salePrice: "10000 đ",
      date: "16/12/2020",
      image: "assets/images/food.jpg",
      name: "Bán tráng trộn hải phòng",
      content: "Hương vị thơm ngon, thích hợp cho mua đông!"),
  ListFastFoodItem(
      read: 0,
      like: 0,
      price: "12000 đ",
      salePrice: "10000 đ",
      date: "16/12/2020",
      image: "assets/images/food.jpg",
      name: "Bán tráng trộn hải phòng",
      content: "Hương vị thơm ngon, thích hợp cho mua đông!"),
  ListFastFoodItem(
      read: 0,
      like: 0,
      price: "12000 đ",
      salePrice: "10000 đ",
      date: "16/12/2020",
      image: "assets/images/food.jpg",
      name: "Bán tráng trộn hải phòng",
      content: "Hương vị thơm ngon, thích hợp cho mua đông!"),
  ListFastFoodItem(
      read: 0,
      like: 0,
      price: "12000 đ",
      salePrice: "10000 đ",
      date: "16/12/2020",
      image: "assets/images/food.jpg",
      name: "Bán tráng trộn hải phòng",
      content: "Hương vị thơm ngon, thích hợp cho mua đông!"),
  ListFastFoodItem(
      read: 0,
      like: 0,
      price: "12000 đ",
      salePrice: "10000 đ",
      date: "16/12/2020",
      image: "assets/images/food.jpg",
      name: "Bán tráng trộn hải phòng",
      content: "Hương vị thơm ngon, thích hợp cho mua đông!"),
  ListFastFoodItem(
      read: 0,
      like: 0,
      price: "12000 đ",
      salePrice: "10000 đ",
      date: "16/12/2020",
      image: "assets/images/food.jpg",
      name: "Bán tráng trộn hải phòng",
      content: "Hương vị thơm ngon, thích hợp cho mua đông!"),
  ListFastFoodItem(
      read: 0,
      like: 0,
      price: "12000 đ",
      salePrice: "10000 đ",
      date: "16/12/2020",
      image: "assets/images/food.jpg",
      name: "Bán tráng trộn hải phòng",
      content: "Hương vị thơm ngon, thích hợp cho mua đông!"),
];
