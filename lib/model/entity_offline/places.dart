class PlacesItem {
  final String image, name, address, intro;
  int idFoodService;
  int idPlace;
  double star, lat, long;
  PlacesItem( {this.idPlace, this.image, this.name, this.intro, this.lat, this.long, this.idFoodService, this.star, this.address});
}

List<PlacesItem> itemPlaces = [
  PlacesItem(
    idPlace: 1,
    name: "Singapore Chicken Rice",
    image: "assets/images/bg1.png",
    star: 4.8,
    idFoodService: 1,
    lat: 20.837639,
    long: 106.700536,
    address: "104-132 Văn Cao, Đằng Giang, Ngô Quyền, Hải Phòng, Việt Nam",
    intro: "Giáng sinh cửa hàng đang giảm giá 10% toàn bộ cửa hàng  Giáng sinh cửa hàng đang giảm giá 10% toàn bộ cửa hàng  Giáng sinh cửa hàng đang giảm giá 10% toàn bộ cửa hàngGiáng sinh cửa hàng đang giảm giá 10% toàn bộ cửa hàng Giáng sinh cửa hàng đang giảm giá 10% toàn bộ cửa hàng  Giáng sinh cửa hàng đang giảm giá 10% toàn bộ cửa hàng"
  ),
  PlacesItem(
    idPlace: 2,
    name: "Singapore Chicken Rice",
    image: "assets/images/bg1.png",
    star: 4.8,
    idFoodService: 1,
    lat: 20.837639,
    long: 106.700536,
    address: "104-132 Văn Cao, Đằng Giang, Ngô Quyền, Hải Phòng, Việt Nam"
  ),
  PlacesItem(
    idPlace: 3,
    name: "Singapore Chicken Rice",
    image: "assets/images/bg1.png",
    star: 4.8,
    idFoodService: 1,
    lat: 20.837639,
    long: 106.700536,
    address: "104-132 Văn Cao, Đằng Giang, Ngô Quyền, Hải Phòng, Việt Nam"
  ),
  PlacesItem(
      idPlace: 4,
    name: "Singapore Chicken Rice",
    image: "assets/images/bg1.png",
    star: 4.8,
    idFoodService: 1,
    lat: 20.837639,
    long: 106.700536,
    address: "104-132 Văn Cao, Đằng Giang, Ngô Quyền, Hải Phòng, Việt Nam"
  ),
  PlacesItem(
      idPlace: 5,
    name: "Singapore Chicken Rice",
    image: "assets/images/bg1.png",
    star: 4.8,
    idFoodService: 1,
    lat: 20.837639,
    long: 106.700536,
    address: "104-132 Văn Cao, Đằng Giang, Ngô Quyền, Hải Phòng, Việt Nam"
  ),


  PlacesItem(
      idPlace: 6,
      name: "Quán phở Cô Loan béo",
      image: "assets/images/noodle.jpg",
      star: 4.9,
      idFoodService: 2,
      lat: 20.839440,
      long: 106.698760,
      address: "104-132 Văn Cao, Đằng Giang, Ngô Quyền, Hải Phòng, Việt Nam"
  ),
  PlacesItem(
      idPlace: 7,
      name: "Quán phở Cô Loan béo",
      image: "assets/images/noodle.jpg",
      star: 4.9,
      idFoodService: 2,
      lat: 20.839440,
      long: 106.698760,
      address: "104-132 Văn Cao, Đằng Giang, Ngô Quyền, Hải Phòng, Việt Nam"
  ),
  PlacesItem(
      idPlace: 8,
      name: "Quán phở Cô Loan béo",
      image: "assets/images/noodle.jpg",
      star: 4.9,
      idFoodService: 2,
      lat: 20.839440,
      long: 106.698760,
      address: "104-132 Văn Cao, Đằng Giang, Ngô Quyền, Hải Phòng, Việt Nam"
  ),


  PlacesItem(
      idPlace: 9,
      name: "HighLand Coffee",
      image: "assets/images/drinks.jpg",
      star: 5.0,
      idFoodService: 3,
      lat: 20.861088,
      long: 106.692328,
      address: "2 P. Phạm Minh Đức, Máy Tơ, Ngô Quyền, Hải Phòng, Việt Nam"
  ),
  PlacesItem(
      idPlace: 10,
      name: "HighLand Coffee",
      image: "assets/images/drinks.jpg",
      star: 5.0,
      idFoodService: 3,
      lat: 20.861088,
      long: 106.692328,
      address: "2 P. Phạm Minh Đức, Máy Tơ, Ngô Quyền, Hải Phòng, Việt Nam"
  ),
  PlacesItem(
      idPlace: 11,
      name: "HighLand Coffee",
      image: "assets/images/coffee.jpg",
      star: 5.0,
      idFoodService: 4,
      lat: 20.861088,
      long: 106.692328,
      address: "2 P. Phạm Minh Đức, Máy Tơ, Ngô Quyền, Hải Phòng, Việt Nam"
  ),

  PlacesItem(
      idPlace: 12,
      name: "Bánh tráng trộn Cô Béo",
      image: "assets/images/ricepaper.jpg",
      star: 5.0,
      idFoodService: 5,
      lat: 20.838492,
      long: 106.695682,
      address: "204 Đường Nguyễn Bình, Đổng Quốc Bình, Ngô Quyền, Hải Phòng, Việt Nam"
  ),
  PlacesItem(
      idPlace: 13,
      name: "Bánh tráng trộn 204",
      image: "assets/images/ricepaper.jpg",
      star: 5.0,
      idFoodService: 5,
      lat: 20.838492,
      long: 106.695682,
      address: "204 Đường Nguyễn Bình, Đổng Quốc Bình, Ngô Quyền, Hải Phòng, Việt Nam"
  ),

];
