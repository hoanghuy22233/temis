class CategoryItem {
  final String image, name, colorImage, background;
  CategoryItem({this.image, this.colorImage, this.name, this.background});
}

List<CategoryItem> itemCategory = [
  CategoryItem(
    name: "dịch vụ hot",
    image: "assets/icons/retweet.png",
    colorImage: "FFE57373",
  ),
  CategoryItem(
    name: "Dịch vụ khuyến mại",
    image: "assets/icons/offer.png",
    colorImage: "FF00E676",
  ),
  CategoryItem(
    name: "Lịch sử",
    image: "assets/icons/historys.png",
    colorImage: "FFFFD180",
  ),
  CategoryItem(
    name: "Cửa hàng mới",
    image: "assets/icons/store-new-badges.png",
    colorImage: "FFFFD180",
  ),
];
