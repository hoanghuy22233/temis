class FoodServiceMenu {
  final String image, name, colorImage, intro;
  final int id;
  FoodServiceMenu({this.id, this.image, this.colorImage, this.name, this.intro});
}

List<FoodServiceMenu> itemFoodService = [
  FoodServiceMenu(
      id: 1,
      name: "Đồ ăn nóng",
      image: "assets/images/tray.png",
      intro: "Nguyên liệu hoàn hảo và đầu bếp giỏi"),
  FoodServiceMenu(
      id: 2,
      name: "Súp / Mì / Phở",
      image: "assets/images/bowl.png",
      intro: "Được làm từ những nguyên liệu truyền thống"),
  FoodServiceMenu(
      id: 3,
      name: "Đồ uống",
      image: "assets/images/cocktail.png",
      intro: "Thử đồ uống tươi một cách thú vị"),
  FoodServiceMenu(
      id: 4,
      name: "Cà phê",
      image: "assets/images/coffee-cup.png",
      intro: "Cà phê được tuyển chọn từ những hạt cà phê chất lượng"),
  FoodServiceMenu(
      id: 5,
      name: "Đồ ăn vặt",
      image: "assets/images/fastfood.png",
      intro: "Đồ ăn vặt với đa dạng các loại"),
];
