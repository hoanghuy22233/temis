import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:temis/model/repo/barrel_repo.dart';
import 'package:temis/presentation/screen/forgot_pass/forgot_password_sc.dart';
import 'package:temis/presentation/screen/forgot_pass_reset/forgot_password_reset_page_sc.dart';
import 'package:temis/presentation/screen/forgot_vefical/forgot_password_verify_sc.dart';
import 'package:temis/presentation/screen/login/login_screen.dart';
import 'package:temis/presentation/screen/login/sc_login.dart';
import 'package:temis/presentation/screen/menu/home/home.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/sc_coffee_store.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/navigation/sc_navigation_fast.dart';
import 'package:temis/presentation/screen/menu/home/home_category/work_service/category_work.dart';
import 'package:temis/presentation/screen/menu/home/work_home/work_navigation/work_navigation.dart';
import 'package:temis/presentation/screen/navigation/sc_navigation.dart';
import 'package:temis/presentation/screen/register/register_sc.dart';
import 'package:temis/presentation/screen/register_vefical/register_verify_sc.dart';
import 'package:temis/presentation/screen/slide_show/slide_show.dart';
import 'package:temis/presentation/screen/splash/sc_splash.dart';

import 'screen/menu/home/home_category/food_service/category_food.dart';

class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String REGISTER = 'register';
  static const String FORGOT_PASS = 'forgot_pass';
  static const String FORGOT_PASS_VERIFY = 'forgot_pass_verify';
  static const String REGISTER_VERIFY = 'register_verify';
  static const String FORGOT_PASS_RESET = 'forgot_pass_reset';
  static const String MENU = 'menu';
  static const String FOOD_SERVICE = 'food_service';
  static const String WORK_SERVICE = 'work_service';

  static const String NAVIGATION_CAFE = 'navigation_cafe';
  static const String NAVIGATION_FOOD = 'navigation_fast_food';

  static const String NAVIGATION = '/navigation';
  static const String SLIDESHOW = '/slideshow';
  static const String WORK_NAVIGATION = '/work_navigation';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case LOGIN:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case NAVIGATION:
        return MaterialPageRoute(builder: (_) => NavigationScreen());
      case WORK_NAVIGATION:
        return MaterialPageRoute(builder: (_) => WorkNavigationScreen());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var homeRepository = RepositoryProvider.of<HomeRepository>(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    var notificationRepository =
        RepositoryProvider.of<NotificationRepository>(context);
    var addressRepository = RepositoryProvider.of<AddressRepository>(context);
    var cartRepository = RepositoryProvider.of<CartRepository>(context);
    var paymentRepository = RepositoryProvider.of<PaymentRepository>(context);
    var invoiceRepository = RepositoryProvider.of<InvoiceRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      LOGIN: (context) => LoginScreenPage(),
      NAVIGATION: (context) => NavigationScreen(),
      WORK_NAVIGATION: (context) => WorkNavigationScreen(),
      REGISTER: (context) => CreateNewAccount(),
      FORGOT_PASS: (context) => ForgotPassword(),
      FORGOT_PASS_VERIFY: (context) => ForgotPassVerifyPage(),
      FORGOT_PASS_RESET: (context) => ForgotPassResetPage(),
      REGISTER_VERIFY: (context) => RegisterVerifyPage(),
      FOOD_SERVICE: (context) => CategoryFood(),
      WORK_SERVICE: (context) => CategoryWork(),
      MENU: (context) => Home(),
      NAVIGATION_FOOD: (context) => NavigationFastScreen(),
      NAVIGATION_CAFE: (context) => CoffeeStore(),
      SLIDESHOW: (context) => MySlideShowPage(),
    };
  }
}
