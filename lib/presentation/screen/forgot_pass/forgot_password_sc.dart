import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/presentation/common_widgets/text-field-input.dart';
import 'package:temis/utils/locale/app_localization.dart';

class ForgotPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
        ),
        title: Text(
          AppLocalizations.of(context).translate('login.forgot_password_s'),
          style: TextStyle(fontSize: 16, color: Colors.black, height: 1.5),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Image.asset(
              "assets/images/header_background.png",
              height: MediaQuery.of(context).size.width * 0.6,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Column(
              children: [
                SizedBox(
                  height: size.height * 0.1,
                ),
                Container(
                  width: size.width * 0.8,
                  child: Text(
                    AppLocalizations.of(context)
                        .translate('forgot_password.sub_message'),
                    style: TextStyle(
                        fontSize: 16, color: Colors.black, height: 1.5),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextInputField(
                  icon: FontAwesomeIcons.envelope,
                  hint: AppLocalizations.of(context).translate('email.name'),
                  inputType: TextInputType.emailAddress,
                  inputAction: TextInputAction.done,
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: size.height * 0.08,
                  width: size.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xff5663ff),
                  ),
                  child: FlatButton(
                    onPressed: () {
                      AppNavigator.navigateForgotPassVerifi();
                    },
                    child: Text(
                      AppLocalizations.of(context).translate('send.pass'),
                      style: TextStyle(
                              fontSize: 16, color: Colors.white, height: 1.5)
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
