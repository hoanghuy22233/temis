import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/model/entity_offline/best_seller.dart';
import 'package:temis/model/entity_offline/list_new_item.dart';
import 'package:temis/model/entity_offline/places.dart';
import 'package:temis/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:temis/utils/locale/app_localization.dart';

class DetailNewNewScreen extends StatefulWidget {
  final int id;

  const DetailNewNewScreen({Key key, this.id}) : super(key: key);
  @override
  _DetailNewNewScreenState createState() =>
      _DetailNewNewScreenState();
}

class _DetailNewNewScreenState extends State<DetailNewNewScreen> {
  bool isReadDetail = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      top: false,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            _background(),
            _buildContent(),
            _buildDirver(),

          ],
        ),
      ),
    ));
  }

  Widget _background() {
    return Stack(
      alignment: Alignment.topLeft,
      children: [
        Container(
          height: MediaQuery.of(context).size.height/4,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: new DecorationImage(
              image: new ExactAssetImage(listNews[widget.id].image),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
            top: 25,
            left: 10,
            child: WidgetAppbarMenuBack(
              color: Colors.black,
            )),
      ],
    );
  }

  Widget _buildContent() {
    return Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        padding: EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              listNews[widget.id].name,
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
        Text(
          listNews[widget.id].date,
          style: TextStyle(fontSize: 12),
        ),
            Divider(
              height: 20,
              thickness: 1,
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Container(
                height: isReadDetail ? null : 50,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(children: <InlineSpan>[
                    TextSpan(
                        text: AppLocalizations.of(context)
                            .translate('intro.detail'),
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.bold)),
                    TextSpan(
                        text: listNews[widget.id].content != null
                            ? listNews[widget.id].content
                            : AppLocalizations.of(context)
                                .translate('intro_null.detail'),
                        style: TextStyle(color: Colors.black, fontSize: 14)),
                  ]),
                ),
              ),
            ),
            listNews[widget.id].content.length > 100
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            isReadDetail = !isReadDetail;
                          });
                        },
                        child: isReadDetail
                            ? Text(
                                "Thu gọn",
                                style: TextStyle(color: Colors.blue),
                              )
                            : Text(
                                "Xem thêm",
                                style: TextStyle(color: Colors.blue),
                              ),
                      ),
                      SizedBox(
                        width: 20,
                      )
                    ],
                  )
                : SizedBox(),
          ],
        ));
  }


  Widget _buildDirver() {
    return Container(
      height: 10,
      color: Colors.grey[300],
    );
  }
}
