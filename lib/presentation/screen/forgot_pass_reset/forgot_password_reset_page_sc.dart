import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:temis/app/constants/navigator/navigator.dart';
import 'package:temis/presentation/common_widgets/password-input.dart';
import 'package:temis/utils/locale/app_localization.dart';

class ForgotPassResetPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            Image.asset(
              "assets/images/header_background.png",
              height: MediaQuery.of(context).size.width * 0.6,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Column(
              children: [
                PasswordInput(
                  icon: FontAwesomeIcons.lock,
                  hint: AppLocalizations.of(context)
                      .translate('forgot_password_reset.new'),
                  inputAction: TextInputAction.next,
                ),
                PasswordInput(
                  icon: FontAwesomeIcons.lock,
                  hint: AppLocalizations.of(context)
                      .translate('register.password'),
                  inputAction: TextInputAction.next,
                ),
                PasswordInput(
                  icon: FontAwesomeIcons.lock,
                  hint: AppLocalizations.of(context)
                      .translate('forgot_password_reset.confirm_password_hint'),
                  inputAction: TextInputAction.done,
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  height: size.height * 0.08,
                  width: size.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xff5663ff),
                  ),
                  child: FlatButton(
                    onPressed: () {
                      AppNavigator.navigateLogin();
                    },
                    child: Text(
                      AppLocalizations.of(context)
                          .translate('forgot_password_reset.update'),
                      style: TextStyle(
                              fontSize: 16, color: Colors.white, height: 1.5)
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
