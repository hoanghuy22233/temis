import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:temis/presentation/screen/login/barrel_login.dart';
import 'package:temis/presentation/screen/login/bloc/bloc.dart';
import 'package:temis/utils/snackbar/barrel_snack_bar.dart';
import 'package:temis/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

class WidgetLoginForm extends StatefulWidget {
  @override
  _WidgetLoginFormState createState() => _WidgetLoginFormState();
}

class _WidgetLoginFormState extends State<WidgetLoginForm> {
  LoginBloc _loginBloc;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool obscurePassword = true;
  bool autoValidate = false;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }
        if (state.isSuccess) {
          GetSnackBarUtils.createSuccess(message: state.message);
        }
        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autoValidate = true;
          });
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Form(
            child: Column(
              children: [
                _buildTextFieldUsername(),
                WidgetSpacer(
                  height: 10,
                ),
                _buildTextFieldPassword(),
                WidgetSpacer(
                  height: 20,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: WidgetLink(
                    style: AppStyle.APP_MEDIUM.copyWith(
                      color: AppColor.PRIMARY,
                      decoration: TextDecoration.underline,
                    ),
                    text: AppLocalizations.of(context)
                        .translate('login.forgot_password'),
                    onTap: () {
                      //   AppNavigator.navigateForgotPassword();
                    },
                  ),
                ),
                WidgetSpacer(
                  height: 20,
                ),
                _buildButtonLogin(state),
                WidgetSpacer(
                  height: 20,
                ),
                Text(
                  AppLocalizations.of(context).translate('login.or'),
                  style: AppStyle.DEFAULT_MEDIUM,
                ),
                WidgetSpacer(
                  height: 20,
                ),
                _buildSocialLogin()
              ],
            ),
          ),
        );
      }),
    );
  }

  _buildSocialLogin() {
    return FractionallySizedBox(
      widthFactor: 0.65,
      child: Row(
        children: [
          Expanded(
            child: WidgetSocialButton(
              image: Image.asset('assets/images/img_facebook.png'),
              text: AppLocalizations.of(context).translate('login.facebook'),
              onTap: () {
                _loginBloc.add(LoginFacebookEvent());
              },
            ),
          ),
          Expanded(
            child: WidgetSocialButton(
              image: Image.asset('assets/images/img_google.png'),
              text: AppLocalizations.of(context).translate('login.google'),
              onTap: () {
                _loginBloc.add(LoginGoogleEvent());
              },
            ),
          )
        ],
      ),
    );
  }

  _buildButtonLogin(LoginState state) {
    return WidgetLoginButton(
      onTap: () {
        if (isRegisterButtonEnabled()) {
          _loginBloc.add(LoginSubmitUsernamePasswordEvent(
            email: _emailController.text.trim(),
            password: _passwordController.text.trim(),
          ));
          FocusScope.of(context).unfocus();
        }
      },
      isEnable: isRegisterButtonEnabled(),
      text: AppLocalizations.of(context).translate('login.title'),
    );
  }

  bool isRegisterButtonEnabled() {
    return _loginBloc.state.isFormValid &&
        isPopulated &&
        !_loginBloc.state.isSubmitting;
  }

  _buildTextFieldPassword() {
    return WidgetLoginInput(
      autovalidate: autoValidate,
      inputController: _passwordController,
      onChanged: (value) {
        _loginBloc.add(LoginPasswordChanged(password: value));
      },
      validator: AppValidation.validatePassword(
          AppLocalizations.of(context).translate('login.password_invalid')),
      hint: AppLocalizations.of(context).translate('login.password_hint'),
      obscureText: obscurePassword,
      endIcon: IconButton(
        icon: Icon(
          obscurePassword
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscurePassword = !obscurePassword;
          });
        },
      ),
    );
  }

  _buildTextFieldUsername() {
    return WidgetLoginInput(
      inputType: TextInputType.emailAddress,
      autovalidate: autoValidate,
      inputController: _emailController,
      onChanged: (value) {
        _loginBloc.add(LoginUsernameChanged(email: value));
      },
      validator: AppValidation.validateUserName(
          AppLocalizations.of(context).translate('login.username_invalid')),
      hint: AppLocalizations.of(context).translate('login.username_hint'),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }
}
