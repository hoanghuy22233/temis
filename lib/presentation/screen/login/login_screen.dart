import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/presentation/common_widgets/password-input.dart';
import 'package:temis/presentation/common_widgets/text-field-input.dart';
import 'package:temis/presentation/screen/login/widget_bottom_register.dart';
import 'package:temis/presentation/screen/login/widget_social_button.dart';
import 'package:temis/utils/locale/app_localization.dart';

class LoginScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            Image.asset(
              "assets/images/header_background.png",
              height: MediaQuery.of(context).size.width * 0.6,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                TextInputField(
                  icon: FontAwesomeIcons.envelope,
                  hint: AppLocalizations.of(context).translate('profile.title'),
                  inputType: TextInputType.emailAddress,
                  inputAction: TextInputAction.next,
                ),
                PasswordInput(
                  icon: FontAwesomeIcons.lock,
                  hint: AppLocalizations.of(context)
                      .translate('register.password'),
                  inputAction: TextInputAction.done,
                ),
                GestureDetector(
                  onTap: () {
                    AppNavigator.navigateForgotPass();
                  },
                  child: Text(
                    AppLocalizations.of(context)
                        .translate('login.forgot_password'),
                    style: TextStyle(
                        fontSize: 16, color: Colors.black, height: 1.5),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  height: size.height * 0.08,
                  width: size.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xff5663ff),
                  ),
                  child: FlatButton(
                    onPressed: () {
                      AppNavigator.navigateSlideShow();
                    },
                    child: Text(
                      AppLocalizations.of(context).translate('register.login'),
                      style: TextStyle(
                              fontSize: 16, color: Colors.white, height: 1.5)
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
              ],
            ),
            _buildSocialLogin(context),
            _buildBottomRegister(),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  _buildSocialLogin(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 0.45,
      child: Row(
        children: [
          Expanded(
            child: WidgetSocialButton(
              image: Image.asset(
                'assets/images/img_facebook.png',
              ),
              text: AppLocalizations.of(context).translate('login.facebook'),
              onTap: () {
                // _loginBloc.add(LoginFacebookEvent());
              },
            ),
          ),
          Expanded(
            child: WidgetSocialButton(
              image: Image.asset(
                'assets/images/google.png',
              ),
              text: AppLocalizations.of(context).translate('login.google'),
              onTap: () {
                //  _loginBloc.add(LoginGoogleEvent());
              },
            ),
          )
        ],
      ),
    );
  }

  _buildBottomRegister() => WidgetBottomRegister();
}
