import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:temis/app/constants/navigator/navigator.dart';
import 'package:temis/presentation/common_widgets/password-input.dart';
import 'package:temis/presentation/common_widgets/text-field-input.dart';
import 'package:temis/utils/locale/app_localization.dart';

class CreateNewAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            SizedBox(
              height: size.width * 0.1,
            ),
            Image.asset(
              "assets/images/header_background.png",
              height: MediaQuery.of(context).size.width * 0.6,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
            ),
            Column(
              children: [
                TextInputField(
                  icon: FontAwesomeIcons.user,
                  hint: AppLocalizations.of(context).translate('register.name'),
                  inputType: TextInputType.name,
                  inputAction: TextInputAction.next,
                ),
                TextInputField(
                  icon: FontAwesomeIcons.envelope,
                  hint: AppLocalizations.of(context).translate('email.name'),
                  inputType: TextInputType.emailAddress,
                  inputAction: TextInputAction.next,
                ),
                PasswordInput(
                  icon: FontAwesomeIcons.lock,
                  hint: AppLocalizations.of(context)
                      .translate('register.password'),
                  inputAction: TextInputAction.next,
                ),
                PasswordInput(
                  icon: FontAwesomeIcons.lock,
                  hint: AppLocalizations.of(context)
                      .translate('forgot_password_reset.confirm_password_hint'),
                  inputAction: TextInputAction.done,
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  height: size.height * 0.08,
                  width: size.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xff5663ff),
                  ),
                  child: FlatButton(
                    onPressed: () {
                      AppNavigator.navigateRegisterVerifi();
                    },
                    child: Text(
                      AppLocalizations.of(context).translate('login.register'),
                      style: TextStyle(
                              fontSize: 16, color: Colors.white, height: 1.5)
                          .copyWith(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      AppLocalizations.of(context)
                          .translate('forgot_password.have_account'),
                      style: TextStyle(
                          fontSize: 16, color: Colors.grey, height: 1.5),
                    ),
                    GestureDetector(
                      onTap: () {
                        AppNavigator.navigateLogin();
                      },
                      child: Text(
                        AppLocalizations.of(context)
                            .translate('register.login'),
                        style: TextStyle(
                                fontSize: 16, color: Colors.grey, height: 1.5)
                            .copyWith(
                                color: Color(0xff5663ff),
                                fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
