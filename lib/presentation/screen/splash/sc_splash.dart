import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/presentation/common_widgets/widget_circle_progress.dart';
import 'package:temis/presentation/common_widgets/widget_logo.dart';

// ignore: must_be_immutable
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  // @override
  void initState() {
    super.initState();
    openLogin();
    // SystemChrome.setEnabledSystemUIOverlays([]);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [_buildLogo()],
        ),
      ),
    );
  }

  _buildLogo() => WidgetLogo(
        height: Get.width * 0.63,
        widthPercent: 0.63,
      );

  _buildProgress() => WidgetCircleProgress();

  void openLogin() async {
    Future.delayed(Duration(seconds: 5), () {
      AppNavigator.navigateLogin();
    });
  }
}
