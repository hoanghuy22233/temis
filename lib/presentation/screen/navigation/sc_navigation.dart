import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/presentation/common_widgets/widget_fab_bottom_nav.dart';
import 'package:temis/presentation/screen/menu/account/prrofile_screen.dart';
import 'package:temis/presentation/screen/menu/home/home.dart';
import 'package:temis/presentation/screen/menu/new/sc_new.dart';
import 'package:temis/presentation/screen/menu/new_page/new_page_screen.dart';
import 'package:temis/presentation/screen/menu/notifications/sc_notifications.dart';

class TabNavigatorRoutes {
//  static const String root = '/';
  static const String home = '/home';
  static const String news = '/news';
  static const String category = '/category';
  static const String account = '/account';
  static const String notification = '/notification';
}

class NavigationScreen extends StatefulWidget {
  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  PageController _pageController;

  List<FABBottomNavItem> _navMenus = List();
  int _selectedIndex = 0;

  List<FABBottomNavItem> _getTab() {
    return List.from([
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.home,
          tabItem: TabItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/homes.png',
          text: "Trang chủ"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.category,
          tabItem: TabItem.category,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/new.png',
          text: "Danh mục"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.news,
          tabItem: TabItem.news,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/writing.png',
          text: "Bảng tin"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.notification,
          tabItem: TabItem.notification,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/notifications.png',
          text: "Thông báo"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.account,
          tabItem: TabItem.account,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/account.png',
          text: "Tài khoản"),
    ]);
  }

  goToPage({int page}) {
    if (page != _selectedIndex) {
      setState(() {
        this._selectedIndex = page;
      });
      _pageController.jumpToPage(_selectedIndex);
    }
  }

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
          width: 60,
          height: 60,
          child: FloatingActionButton(
            elevation: 2,
            shape:
                CircleBorder(side: BorderSide(color: Colors.white, width: 5)),
            onPressed: () {
              goToPage(page: 2);
            },
            child: FractionallySizedBox(
                widthFactor: 0.4,
                child: AspectRatio(
                    aspectRatio: 1,
                    child: Image.asset(
                      'assets/icons/writing.png',
                      color: _selectedIndex == 2
                          ? Colors.green
                          : AppColor.NAV_ITEM_COLOR,
                    ))),
            backgroundColor: Colors.white,
          ),
        ),
        bottomNavigationBar: WidgetFABBottomNav(
          notchedShape: CircularNotchedRectangle(),
          backgroundColor: Colors.white,
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          items: _navMenus,
          selectedColor: Colors.green,
          color: AppColor.NAV_ITEM_COLOR,
        ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (newPage) {
            setState(() {
              this._selectedIndex = newPage;
            });
          },
          children: [
            Home(),
            NewScreen(),
            NewPageScreen(),
            NotificationsScreen(),
            ProfileScreen(),
          ],
        ));
  }
}
