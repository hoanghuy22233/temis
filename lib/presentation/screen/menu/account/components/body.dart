import 'package:flutter/material.dart';

import 'info.dart';
import 'profile_menu_item.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: <Widget>[
          Info(
            image: "assets/images/bds.jpg",
            name: "Temis",
            email: "temisvietnam@gmail.com",
          ),
          SizedBox(height: 10), //20
          ProfileMenuItem(
            iconSrc: "assets/icons/historys.png",
            title: "Lịch sử mua hàng",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/icons/newspaper.png",
            title: "Sổ địa chỉ",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/img_heart_actived.png",
            title: "Yêu thích",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/icons/phone.png",
            title: "Liên hệ",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/connection.png",
            title: "Điều khoản và chính sách",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/languages.png",
            title: "Thay đổi ngôn ngữ",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/help.png",
            title: "Trợ giúp",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/padlock.png",
            title: "Thay đổi mật khẩu",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/icons/exit.png",
            title: "Đăng xuất",
            press: () {},
          ),
        ],
      ),
    );
  }
}
