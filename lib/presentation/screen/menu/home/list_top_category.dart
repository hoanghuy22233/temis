import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/navigator/navigator.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/model/entity_offline/category.dart';
import 'package:temis/utils/color/hex_color.dart';

class ChildTopCategory extends StatelessWidget {
  final int id;
  ChildTopCategory({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: null,
      child: Container(
          width: MediaQuery.of(context).size.width / 5,
          child: Column(
            children: [
              Stack(
                alignment: Alignment.center,
                children: [
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      color: Color(HexColor.getColorFromHex(
                          itemCategory[id].colorImage)),
                      borderRadius: BorderRadius.circular(500),
                    ),
                    child: null,
                  ),
                  Image.asset(
                    '${itemCategory[id].image}',
                    width: 20,
                    height: 20,
                    color: Colors.white,
                  )
                ],
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                itemCategory[id].name.length <= 15
                    ? itemCategory[id].name
                    : itemCategory[id].name.substring(0, 14) + '...',
                style: AppStyle.DEFAULT_SMALL.copyWith(
                    color: AppColor.BLACK, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ],
          )),
    );
  }
}
