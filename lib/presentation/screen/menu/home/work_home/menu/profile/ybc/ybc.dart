import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';


class YBCStartupScreen extends StatefulWidget {
  YBCStartupScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _YBCStartupScreenState createState() => _YBCStartupScreenState();
}

class _YBCStartupScreenState extends State<YBCStartupScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildNameCard(),
          SizedBox(height: 5,),
          SizedBox(height: 5,),
          _buildInfo(),
          SizedBox(height: 10,),
          _buildContact(),
        ],
      ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }

  _buildNameCard(){
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        padding: EdgeInsets.all(15),
        child: Row(
          children: [
            Expanded(
                flex: 2,
                child: Stack(
                  children: [
                    Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.blue[600]
                      ),
                    ),
                    Container(
                      height: 70,
                      width: 70,
                      margin: EdgeInsets.only(top: 5, left: 5),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                          image: new ExactAssetImage('assets/images/logo_ycb.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                )
            ),
            SizedBox(width: 10,),
            Expanded(
                flex: 5,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("YBC Startups", style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith()),
                    SizedBox(width: 10,),
                    Text("CLB Doanh nhân trẻ khởi nghiệp", style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith()),
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }

  _buildInfo(){
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
          padding: EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Cơ sở Hà Nội", style: AppStyle.DEFAULT_SMALL_BOLD.copyWith(),),
              SizedBox(height: 5,),
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Icon(Icons.location_on_outlined, size: 15,)),
                  SizedBox(width: 5,),
                  Expanded(
                      flex: 14,
                      child: Text("Địa chỉ: 14 Đặng Thùy Trâm, Cổ Nhuế, Cầu Giấy, Hà Nội", style: AppStyle.DEFAULT_SMALL.copyWith(), textAlign: TextAlign.justify, maxLines: 2,)),
                ],
              ),
              SizedBox(height: 5,),
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Icon(Icons.email_outlined, size: 15,)),
                  SizedBox(width: 5,),
                  Expanded(
                      flex: 14,
                      child: Text("Email: ybcstartups@gmail.com", style: AppStyle.DEFAULT_SMALL.copyWith(), textAlign: TextAlign.justify, maxLines: 2,)),
                ],
              ),
              SizedBox(height: 5,),
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Icon(Icons.phone_outlined, size: 15,)),
                  SizedBox(width: 5,),
                  Expanded(
                      flex: 14,
                      child: Text("Hotline: 0913915358", style: AppStyle.DEFAULT_SMALL.copyWith(), textAlign: TextAlign.justify, maxLines: 2,)),
                ],
              ),
              SizedBox(height: 15,),
              Text("Cơ sở Hồ Chí Minh", style: AppStyle.DEFAULT_SMALL_BOLD.copyWith(),),
              SizedBox(height: 5,),
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Icon(Icons.email_outlined, size: 15,)),
                  SizedBox(width: 5,),
                  Expanded(
                      flex: 14,
                      child: Text("Email: clbdoanhnhantrehcm@gmail.com", style: AppStyle.DEFAULT_SMALL.copyWith(), textAlign: TextAlign.justify, maxLines: 2,)),
                ],
              ),

            ],
          )
      ),
    );
  }


  _buildContact(){
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Theo dõi chúng tôi", style: AppStyle.DEFAULT_SMALL_BOLD.copyWith(),),
            SizedBox(height: 5,),
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Container(
                      child: Image.asset("assets/images/facebook_square.png", height: 15, width: 15,),
                    ),
                ),
                SizedBox(width: 5,),
                Expanded(
                    flex: 14,
                    child: Text("Facebook", style: AppStyle.DEFAULT_SMALL.copyWith(), textAlign: TextAlign.justify, maxLines: 2,)),
              ],
            ),
            SizedBox(height: 5,),
            Row(
              children: [
                Expanded(
                    flex: 1,
                    child: Container(
                      child: Image.asset("assets/images/youtube-rounded-square-logo.png", color: Colors.black, height: 15, width: 15,),
                    ),
                ),
                SizedBox(width: 5,),
                Expanded(
                    flex: 14,
                    child: Text("Youtube", style: AppStyle.DEFAULT_SMALL.copyWith(), textAlign: TextAlign.justify, maxLines: 2,)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
