import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/profile/information/user_information.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/profile/widget_appbar_profile.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/profile/ybc/ybc.dart';
import 'package:temis/presentation/screen/menu/notifications/comming_notifications/comming.dart';
import 'package:temis/presentation/screen/menu/notifications/widget_notifications_appbar.dart';
import 'package:temis/presentation/screen/menu/notifications/your_notification/your_notifications.dart';

class ProfileWorkScreen extends StatefulWidget {
  final int id;
  ProfileWorkScreen({Key key, this.id}) : super(key: key);

  @override
  _ProfileWorkScreenState createState() => _ProfileWorkScreenState();
}

class _ProfileWorkScreenState extends State<ProfileWorkScreen>
    with SingleTickerProviderStateMixin {
  final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          color: Colors.grey[100],
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                    color: Colors.white,
                    // height: MediaQuery.of(context).size.height * 0.1,
                    child: _buildAppbar(),
                  ),
                  _buildTabBarMenu(),
                ],
              ),

              Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        _userInfo(),
                        _ybcStartup()
                      ],
                    ),
                  )),
            ],
          ),
        ));
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Colors.blue,
    textColor: Colors.white,
    title: "Thông tin và cài đặt",

    right: [Padding(
      padding: const EdgeInsets.only(right: 5),
      child: Icon(Icons.shopping_cart_outlined, color: Colors.white,),
    )],
  );

  Widget _buildTabBarMenu() {
    return new Container(
      margin: EdgeInsets.only(top: 80, left: 20, right: 20),
      decoration: BoxDecoration(
        color: Colors.grey[300],
          borderRadius: BorderRadius.circular(500)),
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: "Thông tin cá nhân",
          ),
          Tab(
            text: "Thông tin YBC Startups",
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        indicator: new BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Colors.white,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 500,
        ),
      ),
    );
  }

  Widget _userInfo() => UserInformation();
  Widget _ybcStartup() => YBCStartupScreen();
}
