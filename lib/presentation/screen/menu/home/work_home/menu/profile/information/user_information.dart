import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';


class UserInformation extends StatefulWidget {
  UserInformation({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _UserInformationState createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          _buildNameCard(),
          SizedBox(height: 5,),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text("Sửa profile > ", style: AppStyle.DEFAULT_SMALL.copyWith(color: Colors.blue)),
            ],
          ),
          SizedBox(height: 5,),
          _buildInfo(),
          SizedBox(height: 10,),
          _buildProductBought(),
          SizedBox(height: 10,),
          _buildChangePassword(),
          SizedBox(height: 30,),
          _buildButtonLogout()
        ],
      ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }

  _buildNameCard(){
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        padding: EdgeInsets.all(15),
        child: Row(
          children: [
            Expanded(
                flex: 2,
                child: Stack(
                  children: [
                    Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.blue[600]
                      ),
                    ),
                    Container(
                      height: 70,
                      width: 70,
                      margin: EdgeInsets.only(top: 5, left: 5),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                          image: new ExactAssetImage('assets/images/avatar_user.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ],
                )
            ),
            Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text("Nguyễn Văn A", style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith()),
                        SizedBox(width: 10,),
                        Container(
                          child: Icon(Icons.wc, size: 15, color: Colors.blue,),
                        )
                      ],
                    ),
                    SizedBox(height: 5,),
                    Card(
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: Text("Dùng thử",style: TextStyle(fontSize: 10, color: Colors.white),)),
                    ),
                    SizedBox(height: 5,),
                    Text("Thông tin tài khoản >")
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }

  _buildInfo(){
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        padding: EdgeInsets.all(15),
        child: Column(
          children: [
            _buildInfoItem("Năm sinh: ", "12/02/1987"),
            _buildInfoItem("Số điện thoại: ", "0123456789"),
            _buildInfoItem("Email: ", "nguyenvana87@gmail.com"),
            _buildInfoItem("Địa chỉ: ", "Ba ĐÌnh - Hà Nội"),
            _buildInfoItem("Công việc đang làm: ", "Quản lý"),
            _buildInfoItem("Ngành nghề: ", "Thời trang"),
            Text("Lời giới thiệu:", style: AppStyle.DEFAULT_SMALL.copyWith()),
            SizedBox(height: 5,),
            Text("I'm currently learning how to build apps using the Flutter SDK and Android Studio. My problem is that I need to add a Divider widget between the 'Administrative' text and the rest of the Card but as you can see in the screenshot below, the divider isn't showing up. I've tried changing the size (In which case the space between the two texts just increases) and I've tried setting the color to see if it was defaulting to transparent on my phone. Nothing works!", style: AppStyle.DEFAULT_SMALL.copyWith(), textAlign: TextAlign.justify,),
          ],
        )
      ),
    );
  }

  _buildInfoItem(String left, String right){
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(left, style: AppStyle.DEFAULT_SMALL.copyWith()),
            Text(right, style: AppStyle.DEFAULT_SMALL.copyWith()),
          ],
        ),
        Divider(
          height: 20,
          thickness: 0.8,
          color: Colors.black,
        )
      ],
    );
  }

  _buildProductBought(){
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        padding: EdgeInsets.all(15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Sản phẩm đã mua", style: AppStyle.DEFAULT_SMALL.copyWith()),
            Text("(7)  >", style: AppStyle.DEFAULT_SMALL.copyWith()),
          ],
        ),
      ),
    );
  }

  _buildChangePassword(){
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      child: Container(
        padding: EdgeInsets.all(15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text("Đổi mật khẩu", style: AppStyle.DEFAULT_SMALL.copyWith()),
            Text(">", style: AppStyle.DEFAULT_SMALL.copyWith()),
          ],
        ),
      ),
    );
  }

  _buildButtonLogout(){
    return GestureDetector(
      onTap: (){

      },
      child: Card(
        color: Colors.red,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
          child: Text("Đăng xuất", style: AppStyle.DEFAULT_MEDIUM.copyWith(color: Colors.white)),
        ),
      ),
    );
  }
}
