import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/work/newsItem.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/DropBox.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_appbar.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_search.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_work_appbar.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/news/worknews_listview.dart';

class WorkNewsScreen extends StatefulWidget {
  @override
  _WorkNewsScreen createState() => _WorkNewsScreen();
}

class _WorkNewsScreen extends State<WorkNewsScreen> {
  @override
  Widget build(BuildContext context) {
    var dropdownValue = 'Sắp xếp theo';
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: Column(
        children: [
          _buildAppbar("Tin tức", "Tìm kiếm tin tức"),
          WidgetWorkSearch(),
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 0, left: 10, right: 10),
              scrollDirection: Axis.vertical,
              itemCount: allNewsItem.length,
              itemBuilder: (context, index) {
                return Container(
                  padding: EdgeInsets.only(top: 10),
                  child: NewsWorkListView(
                    id: index,
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  _builDropBox(String value) {
    return DropBoxAppBar(
      value: value,
    );
  }

  _buildNavigationBar() {
    return WidgetWorkAppbar(
      title: 'tin tức',
    );
  }

  Widget _buildAppbar(String title, String titleSearch) =>
      WidgetAppbarWithSearch(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        title: title,
        titleSearch: titleSearch,
      );
}
