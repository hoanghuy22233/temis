import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/model/entity_offline/work/work_category.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_news_listview.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_work_appbar_home.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/home/work_category.dart';

class WorkHomePage extends StatefulWidget {
  WorkHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _WorkHomePage createState() => _WorkHomePage();
}

class _WorkHomePage extends State<WorkHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _buildNavigationBar(),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  //banner
                  Container(
                    height: MediaQuery.of(context).size.height / 4,
                    child: Swiper(
                      itemBuilder: (BuildContext context, int index) {
                        return new Image.asset(
                          'assets/images/img_nongnghiep.jpg',
                          fit: BoxFit.cover,
                        );
                      },
                      pagination: new SwiperPagination(
                        builder: new DotSwiperPaginationBuilder(
                            color: Colors.grey, activeColor: Colors.white),
                      ),
                      autoplay: true,
                      itemCount: 3,
                      scrollDirection: Axis.horizontal,
                      // control: new SwiperControl(),
                    ),
                  ),
                  Container(
                    height: 80,
                    color: Colors.grey[300],
                    child: GridView.builder(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 5),
                        padding: EdgeInsets.only(top: 10),
                        itemBuilder: (context, index) {
                          return WorkCategory(
                            id: index,
                          );
                        },
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: allWork.length),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  _buildText("Tâm sự đời doanh nhân", 'Xem thêm'),
                  SizedBox(
                    height: 10,
                  ),
                  _buildListView(),
                  SizedBox(
                    height: 25,
                  ),
                  _buildText("Talk Show", 'Xem thêm'),
                  SizedBox(
                    height: 10,
                  ),
                  _buildListView(),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          )
        ],
      ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }

  _buildNavigationBar() {
    return WidgetWorkHomeAppbar(
      title: 'chào anh',
      backgroundColor: Colors.blue,
      textColor: Colors.white,
    );
  }

  _buildListView() {
    return WidgetNewsListView();
  }

  _buildText(String first, String last) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            first,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          Row(
            children: [
              Text(
                last,
                style: TextStyle(
                  color: AppColor.WORK_COLOR,
                  fontSize: 14,
                ),
              ),
              Icon(
                Icons.chevron_right,
                size: 15,
                color: Colors.blue[500],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
