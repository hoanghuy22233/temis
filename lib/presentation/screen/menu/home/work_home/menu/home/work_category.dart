import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/model/entity_offline/work/work_category.dart';


class WorkCategory extends StatefulWidget {
  final int id;
  WorkCategory({Key key, this.id}) : super(key: key);

  @override
  _WorkCategory createState() => _WorkCategory();
}

class _WorkCategory extends State<WorkCategory> {
  bool check = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: null,
      child:  Column(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(500),
                child: Container(
                  height: 40,
                  width: 40,
                  color: Colors.white,
                ),
              ),
              Image.asset(
                "${allWork[widget.id].image}",
                height: 20,
                width: 20,
                color: AppColor.WORK_COLOR,
              ),
            ],
          ),
          SizedBox(height: 5,),
          Text("${allWork[widget.id].title}",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black45,
                fontSize: 12
            ),
          ),
        ],
      ),
    );
  }
}
