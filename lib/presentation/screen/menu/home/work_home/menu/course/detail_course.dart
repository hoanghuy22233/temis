import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/model/entity_offline/work/courseItem.dart';
import 'package:temis/model/entity_offline/work/newsItem.dart';
import 'package:temis/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_work_add_appbar.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_work_appbar.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/course/tab-bar_view/widget_information_course.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/course/tab-bar_view/widget_intro_course.dart';

class DetailCourseScreen extends StatefulWidget {
  final int id;

  const DetailCourseScreen({Key key, this.id}) : super(key: key);

  @override
  _DetailCourseScreenState createState() => _DetailCourseScreenState();
}

class _DetailCourseScreenState extends State<DetailCourseScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     body:Column(
  //       children: [
  //         WidgetWorkAppbar(
  //
  //           title: 'khoá học',
  //           left: [
  //             Padding(
  //               padding: EdgeInsets.only(left: 20),
  //               child: WidgetAppbarMenuBack(),
  //             )
  //           ],
  //         ),
  //         Expanded(
  //           child: Padding(
  //             padding: const EdgeInsets.symmetric(horizontal: 5),
  //             child: Column(
  //               children: [
  //                 Card(
  //                   shape: RoundedRectangleBorder(
  //                       borderRadius:
  //                       BorderRadius.vertical(top: Radius.circular(20))),
  //                   child: Column(
  //                     children: [
  //                       ClipRRect(
  //                           borderRadius: BorderRadius.circular(20),
  //                           child: Image.asset(
  //                             '${allNewsItem[widget.id].image}',
  //                             fit: BoxFit.cover,
  //                           )),
  //                       Padding(
  //                         padding: const EdgeInsets.symmetric(horizontal: 10),
  //                         child: Column(
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             SizedBox(height: 15,),
  //                             Column(
  //                               children: [
  //                                 Text(
  //                                   '${allCourseItems[widget.id].title}',
  //                                   style: TextStyle(fontWeight: FontWeight.bold),
  //                                 ),
  //                                 SizedBox(height: 15,),
  //                                 Row(
  //                                   children: [
  //                                     Image.asset(
  //                                       'assets/images/ic_coins.png',
  //                                       color: Colors.black,
  //                                       height: 20,
  //                                       width: 20,
  //                                     ),
  //                                     SizedBox(width: 10,),
  //                                     Text(
  //                                       '${allCourseItems[widget.id].price}',
  //                                       style: TextStyle(
  //                                           color: Colors.red,
  //                                           fontWeight: FontWeight.bold),
  //                                     ),
  //                                     SizedBox(width: 10,),
  //                                     Text(
  //                                       '${allCourseItems[widget.id].priceSale}',
  //                                       style: TextStyle(
  //                                           decoration:
  //                                           TextDecoration.lineThrough),
  //                                     ),
  //                                     Spacer(),
  //                                     Text(
  //                                         'Chứng chỉ: ${allCourseItems[widget.id].certificate}'),
  //                                   ],
  //                                 ),
  //                                 SizedBox(height: 10,),
  //                                 Row(
  //                                   children: [
  //                                     Image.asset(
  //                                       'assets/images/ic_tag.png',
  //                                       color: Colors.black,
  //                                       height: 20,
  //                                       width: 20,
  //                                     ),
  //                                     SizedBox(width: 10,),
  //                                     Column(
  //                                       crossAxisAlignment:
  //                                       CrossAxisAlignment.start,
  //                                       children: [
  //                                         Text('Thể Loại:'),
  //                                         Text(
  //                                             '${allCourseItems[widget.id].tag}'),
  //                                       ],
  //                                     )
  //                                   ],
  //                                 ),
  //                                 SizedBox(height: 10,),
  //                                 Row(
  //                                   children: [
  //                                     Image.asset(
  //                                       'assets/images/ic_level.png',
  //                                       color: Colors.black,
  //                                       height: 20,
  //                                       width: 20,
  //                                     ),
  //                                     SizedBox(width: 10,),
  //                                     Text(
  //                                         'Trình độ:  ${allCourseItems[widget.id].level}'),
  //                                   ],
  //                                 ),
  //                                 SizedBox(height: 10,),
  //                                 Row(
  //                                   children: [
  //                                     Image.asset(
  //                                       'assets/images/ic_lesson.png',
  //                                       color: Colors.black,
  //                                       height: 20,
  //                                       width: 20,
  //                                     ),
  //                                     SizedBox(width: 10,),
  //                                     Text(
  //                                         'Bài học:  ${allCourseItems[widget.id].lesson}'),
  //                                   ],
  //                                 ),
  //                                 SizedBox(height: 10,),
  //                                 Row(
  //                                   children: [
  //                                     Image.asset(
  //                                       'assets/images/ic_clock.png',
  //                                       color: Colors.black,
  //                                       height: 20,
  //                                       width: 20,
  //                                     ),
  //                                     SizedBox(width: 10,),
  //                                     Text(
  //                                         'Thời gian:  ${allCourseItems[widget.id].time}'),
  //                                   ],
  //                                 ),
  //                                 SizedBox(height: 10,),
  //                                 Row(
  //                                   mainAxisAlignment:
  //                                   MainAxisAlignment.spaceEvenly,
  //                                   children: [
  //                                     Container(
  //                                       alignment: Alignment.center,
  //                                       decoration: BoxDecoration(
  //                                         color: Colors.red,
  //                                         borderRadius: BorderRadius.circular(10),
  //                                       ),
  //                                       height: 30,
  //                                       width: 85,
  //                                       child: Text(
  //                                         'Mua ngay ',
  //                                         style: TextStyle(color: Colors.white),
  //                                       ),
  //                                     ),
  //                                     Container(
  //                                       alignment: Alignment.center,
  //                                       height: 30,
  //                                       width: 140,
  //                                       decoration: BoxDecoration(
  //                                         color: Colors.grey[300],
  //                                         borderRadius: BorderRadius.circular(10),
  //                                       ),
  //                                       child: Row(
  //                                         mainAxisAlignment:
  //                                         MainAxisAlignment.spaceEvenly,
  //                                         children: [
  //                                           Image.asset('assets/images/ic_addtocart.png', color: Colors.black, width: 15,),
  //                                           Text(
  //                                             'Thêm giỏ hàng',
  //                                             style: TextStyle(color: Colors.black),
  //                                           ),
  //                                         ],
  //                                       ),
  //                                     ),
  //                                   ],
  //                                 ),
  //                                 SizedBox(height: 10,),
  //                               ],
  //                             )
  //                           ],
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ),
  //                 Card(
  //                   child: TabBar(
  //                       controller: _tabController,
  //                       indicatorColor: AppColor.WORK_COLOR,
  //                       tabs: [
  //                         Tab(
  //                             child: Text(
  //                               'GIỚI THIỆU KHOÁ HỌC',
  //                               style: TextStyle(
  //                                   fontWeight: FontWeight.bold, fontSize: 12),
  //                             )),
  //                         Tab(
  //                             child: Text(
  //                               'NỘI DUNG KHOÁ HỌC',
  //                               style: TextStyle(
  //                                   fontWeight: FontWeight.bold, fontSize: 12),
  //                             )),
  //                       ]),
  //                 ),
  //                 Container(
  //                   height: 500,
  //                   child: TabBarView(controller: _tabController, children: [
  //                     TabIntro(id: widget.id,),
  //                     TabInfomation(),
  //
  //                   ]),
  //                 )
  //
  //               ],
  //             ),
  //           ),
  //         )
  //       ],
  //     ),
  //
  //
  //
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Stack(
            children: [
              WidgetWorkAppbar(
                backgroundColor: Colors.blue,
                textColor: Colors.white,
                title: 'Chi tiết khóa học',
                left: [
                  WidgetAppbarMenuBack(),
                ],
              ),
              Column(
                children: [
                  Expanded(
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 5, right: 5, top: 80),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(20))),
                              child: Column(
                                children: [
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(
                                        '${allNewsItem[widget.id].image}',
                                        // height:
                                        //     MediaQuery.of(context).size.height *
                                        //         0.3,
                                        fit: BoxFit.cover,
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              '${allCourseItems[widget.id].title}',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Row(
                                              children: [
                                                Image.asset(
                                                  'assets/images/ic_coins.png',
                                                  color: Colors.black,
                                                  height: 20,
                                                  width: 20,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  '${allCourseItems[widget.id].price}',
                                                  style: TextStyle(
                                                      color: Colors.red,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  '${allCourseItems[widget.id].priceSale}',
                                                  style: TextStyle(
                                                      decoration: TextDecoration
                                                          .lineThrough),
                                                ),
                                                Spacer(),
                                                Text(
                                                    'Chứng chỉ: ${allCourseItems[widget.id].certificate}'),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                Image.asset(
                                                  'assets/images/ic_tag.png',
                                                  color: Colors.black,
                                                  height: 20,
                                                  width: 20,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text('Thể Loại:'),
                                                    Text(
                                                        '${allCourseItems[widget.id].tag}'),
                                                  ],
                                                )
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                Image.asset(
                                                  'assets/images/ic_level.png',
                                                  color: Colors.black,
                                                  height: 20,
                                                  width: 20,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                    'Trình độ:  ${allCourseItems[widget.id].level}'),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                Image.asset(
                                                  'assets/images/ic_lesson.png',
                                                  color: Colors.black,
                                                  height: 20,
                                                  width: 20,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                    'Bài học:  ${allCourseItems[widget.id].lesson}'),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              children: [
                                                Image.asset(
                                                  'assets/images/ic_clock.png',
                                                  color: Colors.black,
                                                  height: 20,
                                                  width: 20,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                    'Thời gian:  ${allCourseItems[widget.id].time}'),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: Colors.red,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                  height: 30,
                                                  width: 85,
                                                  child: Text(
                                                    'Mua ngay ',
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                                Container(
                                                  alignment: Alignment.center,
                                                  height: 30,
                                                  width: 140,
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey[300],
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      Image.asset(
                                                        'assets/images/ic_addtocart.png',
                                                        color: Colors.black,
                                                        width: 15,
                                                      ),
                                                      Text(
                                                        'Thêm giỏ hàng',
                                                        style: TextStyle(
                                                            color:
                                                                Colors.black),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Card(
                              child: TabBar(
                                  controller: _tabController,
                                  indicatorColor: AppColor.WORK_COLOR,
                                  tabs: [
                                    Tab(
                                        child: Text(
                                      'GIỚI THIỆU KHOÁ HỌC',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    )),
                                    Tab(
                                        child: Text(
                                      'NỘI DUNG KHOÁ HỌC',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    )),
                                  ]),
                            ),
                            Container(
                              height: 500,
                              child: TabBarView(
                                  controller: _tabController,
                                  children: [
                                    TabIntro(
                                      id: widget.id,
                                    ),
                                    TabInfomation(),
                                  ]),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
          WidgetWorkAddAppbar(
            backgroundColor: Colors.blue,
            textColor: Colors.white,
            title: 'Chi tiết khóa học',
            left: [
              WidgetAppbarMenuBack(),
            ],
          )
        ],
      ),
    );
  }
}
