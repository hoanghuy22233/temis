import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/work/courseItem.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_title.dart';
class TabIntro extends StatelessWidget {
  final int id;

  const TabIntro({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(20))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          WidgetWorkTitle(
            title: 'Giới thiệu',
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Text('${allCourseItems[id].intro}'),
          ),
          WidgetWorkTitle(
            title: 'Thông tin giảng viên',
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25),
            child: Text('${allCourseItems[id].intro}'),
          ),
        ],
      ),
    );
  }
}
