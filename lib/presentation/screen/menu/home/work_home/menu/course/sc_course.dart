import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/work/newsItem.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_appbar.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_search.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_work_appbar.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/course/course_gridview.dart';

class WorkCourseScreen extends StatefulWidget {
  @override
  _WorkCourseScreen createState() => _WorkCourseScreen();
}

class _WorkCourseScreen extends State<WorkCourseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: Column(
        children: [
          _buildAppbar("Khóa học", "Tìm kiếm Khóa học"),
          WidgetWorkSearch(),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: GridView.builder(
              padding: EdgeInsets.symmetric(horizontal: 10),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  childAspectRatio: 0.8,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10),
              scrollDirection: Axis.vertical,
              itemCount: allNewsItem.length,
              itemBuilder: (context, index) {
                return Container(
                  child: CourseWorkGridView(
                    id: index,
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  _buildNavigationBar() {
    return WidgetWorkAppbar(
      title: 'khoá học',
    );
  }

  Widget _buildAppbar(String title, String titleSearch) =>
      WidgetAppbarWithSearch(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        title: title,
        titleSearch: titleSearch,
      );
}
