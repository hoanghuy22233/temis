import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/model/entity_offline/work/planItem.dart';
import 'package:temis/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_appbar.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/plan_business/widget_button.dart';
import 'package:temis/presentation/screen/pdf/pdf.dart';

class DetailPlanBusinessScreen extends StatefulWidget {
  final int id;
  const DetailPlanBusinessScreen({Key key, this.id}) : super(key: key);

  @override
  _DetailPlanBusinessScreenState createState() =>
      _DetailPlanBusinessScreenState();
}

class _DetailPlanBusinessScreenState extends State<DetailPlanBusinessScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  String assetPDFPath = "";
  String urlPDFPath = "";

  @override
  void initState() {
    super.initState();

    getFileFromAsset("assets/pdf/pro.pdf").then((f) {
      setState(() {
        assetPDFPath = f.path;
        print(assetPDFPath);
      });
    });

    // getFileFromUrl("http://www.pdf995.com/samples/pdf.pdf").then((f) {
    //   setState(() {
    //     urlPDFPath = f.path;
    //     print(urlPDFPath);
    //   });
    // });
  }

  Future<File> getFileFromAsset(String asset) async {
    try {
      var data = await rootBundle.load(asset);
      var bytes = data.buffer.asUint8List();
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/pro.pdf");

      File assetFile = await file.writeAsBytes(bytes);
      return assetFile;
    } catch (e) {
      throw Exception("Error opening asset file");
    }
  }

  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          _buildAppbar(
              "Mẫu kế hoạch kinh doanh", "Tìm kiếm theo ngành hoặc tác giả"),
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Column(
                  children: [
                    Card(
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(20))),
                      child: Column(
                        children: [
                          ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: Image.asset(
                                '${planBusinessItem[widget.id].image}',
                                fit: BoxFit.cover,
                              )),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        '${planBusinessItem[widget.id].name}',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            height: 10,
                                            width: 10,
                                            child: Image.asset(
                                                "assets/images/skyline.png"),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            'Quy mô: ${planBusinessItem[widget.id].type}',
                                            style: TextStyle(fontSize: 14),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            height: 10,
                                            width: 10,
                                            child: Image.asset(
                                                "assets/images/money.png"),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          planBusinessItem[widget.id]
                                                      .priceNotSale !=
                                                  null
                                              ? Row(
                                                  children: [
                                                    Text(
                                                      AppValue.APP_MONEY_FORMAT
                                                          .format(
                                                              planBusinessItem[
                                                                      widget.id]
                                                                  .priceNotSale),
                                                      style: TextStyle(
                                                          fontSize: 12,
                                                          color: Colors.red,
                                                          decoration:
                                                              TextDecoration
                                                                  .lineThrough),
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                    ),
                                                    SizedBox(
                                                      width: 5,
                                                    ),
                                                  ],
                                                )
                                              : SizedBox(),
                                          Text(
                                            planBusinessItem[widget.id].price !=
                                                    null
                                                ? AppValue.APP_MONEY_FORMAT
                                                    .format(planBusinessItem[
                                                            widget.id]
                                                        .price)
                                                : "Miễn phí",
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: Colors.red,
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            height: 10,
                                            width: 10,
                                            child: Image.asset(
                                                "assets/images/writer.png"),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            'Tác giả: ${planBusinessItem[widget.id].author}',
                                            style: TextStyle(fontSize: 12),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            height: 10,
                                            width: 10,
                                            child: Image.asset(
                                                "assets/images/suitcase.png"),
                                          ),
                                          SizedBox(
                                            width: 5,
                                          ),
                                          Text(
                                            'Ngành: ${planBusinessItem[widget.id].branch}',
                                            style: TextStyle(fontSize: 12),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      _buildButtonBuy(),
                                      _buildButtonDownload(
                                          assetPDFPath, context),
                                      _buildButtonCart(),
                                      _buildButtonsample(),
                                      SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 5,
                    ),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.only(left: 15, right: 10, top: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          _buildContent(
                              "Giới thiệu", planBusinessItem[widget.id].intro),
                          _buildContent("Nội dung mẫu kinh doanh",
                              planBusinessItem[widget.id].content),
                          _buildContent("Thông tin tác giả",
                              planBusinessItem[widget.id].infoAuthor),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

Widget _buildAppbar(String title, String titleSearch) => WidgetAppbarWithSearch(
      backgroundColor: Colors.blue,
      textColor: Colors.white,
      left: [
        WidgetAppbarMenuBack(),
      ],
      title: title,
      titleSearch: titleSearch,
    );

Widget _buildButtonBuy() => WidgetButton(
      text: "Mua ngay",
      textColor: Colors.white,
      image: Icon(
        Icons.attach_money_outlined,
        color: Colors.white,
      ),
      backgroundColor: Colors.red,
      height: 30,
      width: 100,
      isEnable: true,
      onTap: () {},
    );

Widget _buildButtonDownload(String assetPDFPath, BuildContext context) =>
    WidgetButton(
      text: "Tải về",
      textColor: Colors.white,
      image: Icon(
        Icons.cloud_download_outlined,
        color: Colors.white,
      ),
      backgroundColor: Colors.red,
      height: 30,
      width: 100,
      isEnable: true,
      onTap: () {
        if (assetPDFPath != null) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PdfViewPage(path: assetPDFPath)));
        }
      },
    );

Widget _buildButtonCart() => WidgetButton(
      text: "Giỏ hàng",
      image: Icon(
        Icons.add_shopping_cart,
        color: Colors.black,
      ),
      backgroundColor: Colors.grey[400],
      height: 30,
      width: 100,
      isEnable: true,
      onTap: () {},
    );

Widget _buildButtonsample() => WidgetButton(
      text: "Tạo mẫu",
      image: Icon(
        Icons.note_add_outlined,
        color: Colors.black,
      ),
      backgroundColor: Colors.grey[400],
      height: 30,
      width: 100,
      isEnable: true,
      onTap: () {},
    );

Widget _buildContent(String title, String content) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Row(
        children: [
          Icon(
            Icons.circle,
            size: 10,
            color: Colors.blue,
          ),
          SizedBox(
            width: 5,
          ),
          Text(title, style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith()),
        ],
      ),
      SizedBox(
        height: 5,
      ),
      Container(
          padding: EdgeInsets.only(left: 15, bottom: 15),
          child: Text(
            content,
            style: AppStyle.DEFAULT_SMALL.copyWith(),
            textAlign: TextAlign.justify,
          )),
    ],
  );
}
