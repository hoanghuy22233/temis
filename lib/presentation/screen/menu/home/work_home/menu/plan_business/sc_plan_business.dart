import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/work/planItem.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/widget_appbar.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/plan_business/plan_business_listview.dart';

class PlanBusinessScreen extends StatefulWidget {
  @override
  _PlanBusinessScreenState createState() => _PlanBusinessScreenState();
}

class _PlanBusinessScreenState extends State<PlanBusinessScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildAppbar(
            "Mẫu kế hoạch kinh doanh", "Tìm kiếm theo ngành hoặc tác giả"),
        Expanded(
          child: ListView.builder(
            padding: EdgeInsets.only(top: 0, left: 10, right: 10),
            scrollDirection: Axis.vertical,
            itemCount: planBusinessItem.length,
            itemBuilder: (context, index) {
              return PlanBusinessListView(
                id: index,
              );
            },
          ),
        )
      ],
    );
  }

  Widget _buildAppbar(String title, String titleSearch) =>
      WidgetAppbarWithSearch(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        title: title,
        titleSearch: titleSearch,
      );
}
