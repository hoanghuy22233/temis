import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/presentation/common_widgets/widget_fab_bottom_nav.dart';
import 'package:temis/presentation/screen/menu/account/prrofile_screen.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/course/sc_course.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/home/home.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/news/detail_news.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/news/sc_news.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/plan_business/sc_plan_business.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/profile/sc_profile.dart';
import 'package:temis/presentation/screen/menu/home/work_home/work_navigation/widget_work_bottom_nav.dart';


class TabNavigatorRoutesWork {
//  static const String root = '/';
  static const String home = '/home';
  static const String news = '/news';
  static const String course = '/course';
  static const String contract = '/contract';
  static const String person = '/person';
}

class WorkNavigationScreen extends StatefulWidget {
  @override
  _WorkNavigationScreen createState() => _WorkNavigationScreen();
}

class _WorkNavigationScreen extends State<WorkNavigationScreen> {
  PageController _pageController;

  List<WorkFABBottomNavItem> _navMenus = List();
  int _selectedIndex = 2;

  List<WorkFABBottomNavItem> _getTab() {
    return List.from([

      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.news,
          tabItem: TabWorkItem.news,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/ic_news.png',
          text: 'Tin tức'),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.course,
          tabItem: TabWorkItem.course,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/ic_course.png',
          text: "Bảng tin"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.home,
          tabItem: TabWorkItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/homes.png',
          text: "Trang chủ"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.contract,
          tabItem: TabWorkItem.contract,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/ic_contract.png',
          text: "Thông báo"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.person,
          tabItem: TabWorkItem.person,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/account.png',
          text: "Tài khoản"),
    ]);
  }

  goToPage({int page}) {
    if (page != _selectedIndex) {
      setState(() {
        this._selectedIndex = page;
      });
      _pageController.jumpToPage(_selectedIndex);
    }
  }

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        // floatingActionButton: Container(
        //   width: 60,
        //   height: 60,
        //   child: FloatingActionButton(
        //     elevation: 2,
        //     shape:
        //         CircleBorder(side: BorderSide(color: Colors.white, width: 5)),
        //     onPressed: () {
        //       goToPage(page: 2);
        //     },
        //     child: FractionallySizedBox(
        //         widthFactor: 0.4,
        //         child: AspectRatio(
        //             aspectRatio: 1,
        //             child: Image.asset(
        //               'assets/icons/writing.png',
        //               color: _selectedIndex == 2
        //                   ? AppColor.NAV_ITEM_SELECTED_COLOR
        //                   : AppColor.NAV_ITEM_COLOR,
        //             ))),
        //     backgroundColor: Color(0xff66FFCC),
        //   ),
        // ),
        bottomNavigationBar: WorkWidgetFABBottomNav(
          notchedShape: CircularNotchedRectangle(),
          backgroundColor: Colors.white,
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          centerItemText: "A",
          items: _navMenus,
          selectedColor: AppColor.WORK_COLOR,
          color: AppColor.NAV_ITEM_COLOR,
        ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (newPage) {
            setState(() {
              this._selectedIndex = newPage;
            });
          },
          children: [

            WorkNewsScreen(),
            WorkCourseScreen(),
            WorkHomePage(),
            PlanBusinessScreen(),
            ProfileWorkScreen(),
          ],
        ));
  }
}
