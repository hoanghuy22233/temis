import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetWorkTitle extends StatelessWidget {
  final String title;


  const WidgetWorkTitle({Key key, this.title}) : super(key: key);@override
  Widget build(BuildContext context) {
    return Stack(children: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
        child: Text(
          title,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
      ),
      Positioned(
        top: 10,
        left: 10,
        child: Container(
          height: 10,
          width: 5,
          color: Colors.blue,
        ),
      )
    ]);
  }
}
