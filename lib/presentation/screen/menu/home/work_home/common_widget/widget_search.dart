import 'package:flutter/material.dart';
import 'package:temis/presentation/screen/menu/home/work_home/common_widget/DropBox.dart';

class WidgetWorkSearch extends StatefulWidget {
  @override
  _WidgetWorkSearch createState() => _WidgetWorkSearch();
}

class _WidgetWorkSearch extends State<WidgetWorkSearch> {
  @override
  Widget build(BuildContext context) {
    String dropdownValue;
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15),
      height: 35,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 6,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25),
                color: Colors.white,
              ),
              child: _builDropBox('Tất cả'),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Image.asset(
            'assets/images/ic_grid.png',
            height: 15,
          ),
          SizedBox(
            width: 10,
          ),
          DropdownButton<String>(
            value: dropdownValue = 'Sắp xếp theo',
            icon: Icon(Icons.arrow_drop_down),
            iconSize: 24,
            elevation: 16,
            underline: SizedBox(),
            style: TextStyle(color: Colors.black),
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
            },
            items: <String>[dropdownValue]
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          )
        ],
      ),
    );
  }

  _builDropBox(String value) => DropBoxAppBar(
        value: value,
      );
}
