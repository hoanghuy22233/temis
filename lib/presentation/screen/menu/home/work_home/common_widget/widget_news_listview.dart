import 'package:flutter/cupertino.dart';
import 'package:temis/model/entity_offline/work/businessmen_items.dart';
import 'package:temis/presentation/screen/menu/home/work_home/menu/home/businessmen_listview.dart';

class WidgetNewsListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 225,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.symmetric(horizontal: 15),
        itemCount: allBusiness.length,
        itemBuilder: (context, index) {
          return Container(
            child: BusinessmenListView(
              id: index,
            ),
          );
        },
      ),
    );
  }
}
