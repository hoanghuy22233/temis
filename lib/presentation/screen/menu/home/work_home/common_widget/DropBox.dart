
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
class DropBoxAppBar extends StatefulWidget {
  
  final String value;
  
  const DropBoxAppBar({Key key, this.value}) : super(key: key);
  @override
  _DropBoxAppBarState createState() => _DropBoxAppBarState();
}

class _DropBoxAppBarState extends State<DropBoxAppBar> {
  @override
  Widget build(BuildContext context) {
    String dropdownValue = widget.value;
    return DropdownButton<String>(
      value: dropdownValue,
      icon: Icon(Icons.arrow_drop_down),
      iconSize: 24,
      elevation: 16,
      isExpanded: true,
      underline: SizedBox(),
      style: TextStyle(color: Colors.black),
      onChanged: (String newValue) {
        setState(() {
          dropdownValue = newValue;
        });
      },
      items: <String>[dropdownValue]
          .map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }
}
