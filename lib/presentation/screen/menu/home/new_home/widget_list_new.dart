import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/new.dart';
import 'package:temis/presentation/screen/menu/home/new_home/new_item.dart';

class WidgetListNew extends StatelessWidget {
  final Function cancelBackToHome;
  WidgetListNew({this.cancelBackToHome});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 10, left: 5, right: 5),
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: WidgetNewItem(
              id: index,cancelBackToHome: cancelBackToHome,
            ),
          );
        },
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            // crossAxisSpacing: 10,
            // mainAxisSpacing: 5,
            childAspectRatio: 0.9),
        physics: NeverScrollableScrollPhysics(),
        itemCount: itemNew.length);
  }
}
