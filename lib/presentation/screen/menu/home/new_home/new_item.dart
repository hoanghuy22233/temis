import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/model/entity_offline/new.dart';

class WidgetNewItem extends StatefulWidget {
  final Function cancelBackToHome;
  final int id;
  WidgetNewItem({Key key, this.id,this.cancelBackToHome}) : super(key: key);

  @override
  _WidgetNewItemState createState() => _WidgetNewItemState();
}

class _WidgetNewItemState extends State<WidgetNewItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.cancelBackToHome,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Container(
              child: Image.asset(
                itemNew[widget.id].image,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            itemNew[widget.id].name.length <= 20
                ? itemNew[widget.id].name
                : itemNew[widget.id].name.substring(0, 20) + '...',
            style: AppStyle.DEFAULT_SMALL.copyWith(color: AppColor.BLACK),
            textAlign: TextAlign.center,
            maxLines: 2,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            itemNew[widget.id].nameStore.length <= 20
                ? itemNew[widget.id].nameStore
                : itemNew[widget.id].nameStore.substring(0, 20) + '...',
            style: AppStyle.DEFAULT_SMALL.copyWith(color: AppColor.BLACK),
            textAlign: TextAlign.center,
            maxLines: 2,
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                itemNew[widget.id].date.length <= 14
                    ? itemNew[widget.id].date
                    : itemNew[widget.id].date.substring(0, 14) + '...',
                style: AppStyle.DEFAULT_SMALL.copyWith(color: AppColor.BLACK),
                textAlign: TextAlign.center,
                maxLines: 2,
              ),
            ],
          )
        ],
      ),
    );
  }
}
