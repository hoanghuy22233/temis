import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/model/entity_offline/banner.dart';
import 'package:temis/model/entity_offline/category.dart';
import 'package:temis/presentation/common_widgets/widget_search.dart';
import 'package:temis/presentation/screen/menu/home/home_category/work_service/category_work.dart';
import 'package:temis/presentation/screen/menu/home/widget_item_category.dart';
import 'package:temis/utils/locale/app_localization.dart';
import 'banner_home/widget_banner_home.dart';
import 'home_category/bike_service/category_bike.dart';
import 'home_category/estate_service/category_estate.dart';
import 'home_category/etrolic_service/category_etrolic.dart';
import 'home_category/fashion_service/category_fashion.dart';
import 'home_category/food_service/category_food.dart';
import 'home_category/furniture_service/category_furniture.dart';
import 'home_category/game_service/category_game.dart';
import 'home_category/learning_service/category_learning.dart';
import 'home_category/pet_service/category_pet.dart';
import 'home_category/trarvel_service/category_trarvel.dart';
import 'home_category/work_service/category_work.dart';
import 'list_top_category.dart';
import 'new_home/widget_list_new.dart';

class Home extends StatefulWidget {
  @override
  HomeState createState() => new HomeState();
}

class HomeState extends State<Home> {
  double heght;
  int type;
  PageController pageController = PageController();
  Duration _animationDuration = Duration(milliseconds: 500);

  void _changePage(int page) {
    pageController.animateToPage(page,
        duration: _animationDuration, curve: Curves.easeIn);
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Material(
          color: Colors.transparent,
          child: Container(
            color: Colors.black,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: pageController,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          NestedScrollView(
                              headerSliverBuilder: (BuildContext context,
                                  bool innerBoxIsScrolled) {
                                return <Widget>[
                                  SliverAppBar(
                                    expandedHeight:
                                        MediaQuery.of(context).size.height *
                                            0.52,
                                    floating: false,
                                    pinned: true,
                                    flexibleSpace: FlexibleSpaceBar(
                                      centerTitle: true,
                                      background: _buildContent(),
                                    ),
                                  ),
                                ];
                              },
                              body: SingleChildScrollView(
                                child: _buildCategory(),
                              )),
                          Container(
                            color: Colors.white,
                            height: MediaQuery.of(context).size.height * 0.1,
                            child: _buildNavigationBar(),
                          ),
                        ],
                      ),
                      CategoryFood(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryEtrolic(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryBike(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryWork(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryPet(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryFashion(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryFurniture(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryTrarvel(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryEstate(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryGame(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                      CategoryLearning(
                        cancelBackToHome: () {
                          _changePage(0);
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
//            ),
//          ),
        ),
      ],
    );
  }

  Widget _buildSearch() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Card(
          elevation: 2,
          child: Stack(
            alignment: Alignment.centerLeft,
            children: [
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: Text(
                    AppLocalizations.of(context).translate('search.app'),
                    style: TextStyle(color: Colors.grey[300], fontSize: 14),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20),
                child: Icon(
                  Icons.search,
                  color: Colors.grey[300],
                ),
              )
            ],
          ),
        ),
        Image.asset(
          "assets/icons/headphones.png",
          height: 30,
          width: 30,
          color: AppColor.NAV_ITEM_COLOR,
        )
      ],
    );
  }

  Widget _buildMenu() {
    return Container(
      height: MediaQuery.of(context).size.height / 8,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: itemCategory.length + 1,
        itemBuilder: (context, index) {
          if (index == itemCategory.length) {
            return GestureDetector(
              onTap: () async {
                //  displayBottomSheet(context);
              },
              child: Container(
                  child: Column(
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(500),
                        ),
                        child: null,
                      ),
                      Image.asset(
                        'assets/images/ic_more.png',
                        width: 20,
                        height: 20,
                        color: Colors.white,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    'Xem thêm',
                    textAlign: TextAlign.center,
                    style: AppStyle.DEFAULT_SMALL.copyWith(
                        color: AppColor.BLACK, fontWeight: FontWeight.bold),
                  ),
                ],
              )),
            );
          }
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: ChildTopCategory(
              id: index,
            ),
          );
        },
      ),
    );
  }

  Widget _buildCategory() {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              "Danh mục",
              style: TextStyle(fontSize: 16)
                  .copyWith(color: AppColor.BLACK, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 10),
            height: MediaQuery.of(context).size.height * 0.35,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                SizedBox(
                  width: 20,
                ),
                WidgetCategiry(
                  onTap: () {
                    _changePage(1);
                  },
                  onTapTwo: () {
                    _changePage(2);
                  },
                  image: Image.asset(
                    "assets/images/food.jpg",
                    fit: BoxFit.cover,
                  ),
                  imageTwo: Image.asset(
                    "assets/images/electronic.jpg",
                    fit: BoxFit.cover,
                  ),
                  text: AppLocalizations.of(context).translate('category.food'),
                  textTwo:
                      AppLocalizations.of(context).translate('category.it'),
                ),
                SizedBox(
                  width: 10,
                ),
                WidgetCategiry(
                  onTap: () {
                    _changePage(3);
                  },
                  onTapTwo: () {
                    _changePage(4);
                  },
                  image: Image.asset(
                    "assets/images/bike.jpg",
                    fit: BoxFit.cover,
                  ),
                  imageTwo: Image.asset(
                    "assets/images/work.jpg",
                    fit: BoxFit.cover,
                  ),
                  text: AppLocalizations.of(context).translate('category.bike'),
                  textTwo:
                      AppLocalizations.of(context).translate('category.work'),
                ),
                SizedBox(
                  width: 10,
                ),
                WidgetCategiry(
                  onTap: () {
                    _changePage(5);
                  },
                  onTapTwo: () {
                    _changePage(6);
                  },
                  image: Image.asset(
                    "assets/images/dogg.jpg",
                    fit: BoxFit.cover,
                  ),
                  imageTwo: Image.asset(
                    "assets/images/fashion.jpg",
                    fit: BoxFit.cover,
                  ),
                  text: AppLocalizations.of(context).translate('category.dog'),
                  textTwo:
                      AppLocalizations.of(context).translate('category.mask'),
                ),
                SizedBox(
                  width: 10,
                ),
                WidgetCategiry(
                  onTap: () {
                    _changePage(7);
                  },
                  onTapTwo: () {
                    _changePage(8);
                  },
                  image: Image.asset(
                    "assets/images/noi-that.jpg",
                    fit: BoxFit.cover,
                  ),
                  imageTwo: Image.asset(
                    "assets/images/trarvel.jpg",
                    fit: BoxFit.cover,
                  ),
                  text: AppLocalizations.of(context)
                      .translate('category.house_ware'),
                  textTwo:
                      AppLocalizations.of(context).translate('category.travel'),
                ),
                SizedBox(
                  width: 10,
                ),
                WidgetCategiry(
                  onTap: () {
                    _changePage(9);
                  },
                  onTapTwo: () {
                    _changePage(10);
                  },
                  image: Image.asset(
                    "assets/images/bds.jpg",
                    fit: BoxFit.cover,
                  ),
                  imageTwo: Image.asset(
                    "assets/images/images_game.jpg",
                    fit: BoxFit.cover,
                  ),
                  text: AppLocalizations.of(context)
                      .translate('category.real_estate'),
                  textTwo:
                      AppLocalizations.of(context).translate('category.game'),
                ),
                SizedBox(
                  width: 10,
                ),
                WidgetCategiry(
                  onTap: () {
                    _changePage(11);
                  },
                  onTapTwo: null,
                  image: Image.asset(
                    "assets/images/images.jpg",
                    fit: BoxFit.cover,
                  ),
                  text: AppLocalizations.of(context)
                      .translate('category.learning'),
                  // textTwo:
                  // AppLocalizations.of(context).translate('category.game'),
                ),
                SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
          _buildDirver(),
          SizedBox(
            height: 20,
          ),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              "Tin dành cho bạn",
              style: TextStyle(fontSize: 16)
                  .copyWith(color: AppColor.BLACK, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            child: WidgetListNew(
              cancelBackToHome: () {
                _changePage(1);
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Container(
      padding: EdgeInsets.only(
        top: 70,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 4,
            child: Swiper(
              itemBuilder: (BuildContext context, int index) {
                return ChildBannerHome(
                  id: index,
                );
              },
              pagination: new SwiperPagination(
                builder: new DotSwiperPaginationBuilder(
                    color: Colors.grey, activeColor: Colors.white),
              ),
              autoplay: true,
              itemCount: itemBanner.length,
              scrollDirection: Axis.horizontal,
              // control: new SwiperControl(),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          _buildMenu(),
          _buildDirver(),
        ],
      ),
    );
  }

  Widget _buildDirver() {
    return Container(
      height: 10,
      color: Colors.grey[300],
    );
  }

  Widget _buildPadding() {
    return SizedBox(
      height: heght,
    );
  }

  _buildNavigationBar() {
    return SafeArea(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Hero(
              tag: 'search',
              child: GestureDetector(
                onTap: () {
                  //AppNavigator.navigateSearch();
                },
                child: AbsorbPointer(
                  absorbing: true,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: AppValue.APP_HORIZONTAL_PADDING),
                    child: WidgetSearch(),
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Image.asset(
              "assets/icons/headphones.png",
              height: 30,
              width: 30,
              color: AppColor.NAV_ITEM_COLOR,
            ),
          ),
          SizedBox(width: 10)
        ],
      ),
    );
  }
}
