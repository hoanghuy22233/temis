import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/model/entity_offline/work_service.dart';
class WorkListView extends StatelessWidget {
  final int id;

  const WorkListView({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=> AppNavigator.navigateWorkService(),
      child: Card(
          color: Colors.white,
          elevation: 1,
          child: Container(
            margin: EdgeInsets.all(5),
            height: 130,
            width: MediaQuery.of(context).size.width,
            //padding: EdgeInsets.all(5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                    flex: 2,
                    child: Container(
                      height: 130,
                      width: 80,
                      decoration: BoxDecoration(
                        borderRadius:
                        BorderRadius.all(Radius.circular(5)),
                        image: new DecorationImage(
                          image: new ExactAssetImage(
                              itemWorkService[id].image),
                          fit: BoxFit.cover,
                        ),
                      ),
                    )),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                    flex: 3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          itemWorkService[id].name,
                          style: TextStyle(fontSize: 16),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Địa chỉ: ${itemWorkService[id].intro}",
                          style: TextStyle(fontSize: 14),
                        ),
                      ],
                    ))
              ],
            ),
          )),

    );
  }
}
