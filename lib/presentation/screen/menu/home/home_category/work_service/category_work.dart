import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/model/entity_offline/work_service.dart';
import 'package:temis/presentation/common_widgets/widget_appbar_menu.dart';
import 'package:temis/presentation/common_widgets/widget_search.dart';
import 'package:temis/presentation/screen/menu/home/home_category/work_service/work_listview.dart';

class CategoryWork extends StatefulWidget {
  final Function cancelBackToHome;
  final int type;
  CategoryWork({this.cancelBackToHome, this.type});

  @override
  _CategoryWork createState() => _CategoryWork();
}

class _CategoryWork extends State<CategoryWork> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Flexible(
            child: Container(
              color: Colors.white,
              height: MediaQuery.of(context).size.height * 0.1,
              child: _buildNavigationBar(),
            ),
          ),
          Expanded(flex: 9, child: _buildListMenu())
        ],
      ),
    );
  }

  _buildNavigationBar() {
    return SafeArea(
      child: Row(
        children: <Widget>[
          Container(
              margin:
                  const EdgeInsets.only(left: AppValue.ACTION_BAR_HEIGHT * 0.2),
              height: 25,
              width: 25,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.grey[100]),
              child: WidgetAppbarMenu(
                icon: Image.asset(
                  'assets/icons/ic_back.png',
                  color: Colors.black,
                ),
                onTap: widget.cancelBackToHome,
              )),
          Expanded(
            child: Hero(
              tag: 'search',
              child: GestureDetector(
                onTap: () {
                  //AppNavigator.navigateSearch();
                },
                child: AbsorbPointer(
                  absorbing: true,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: AppValue.APP_HORIZONTAL_PADDING),
                    child: WidgetSearch(),
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Image.asset(
              "assets/icons/headphones.png",
              height: 30,
              width: 30,
              color: AppColor.NAV_ITEM_COLOR,
            ),
          ),
          SizedBox(width: 10)
        ],
      ),
    );
  }

  Widget _buildListMenu() {
    return ListView.builder(
      padding: EdgeInsets.zero,
      scrollDirection: Axis.vertical,
      itemCount: itemWorkService.length,
      itemBuilder: (context, index) {
        return Container(
          child: WorkListView(
            id: index,
          ),
        );
      },
    );
  }
}
