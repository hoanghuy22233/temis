import 'package:flutter/material.dart';
import 'package:im_animations/im_animations.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/model/entity_offline/best_seller.dart';
import 'package:temis/model/entity_offline/places.dart';
import 'package:temis/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/detail_item_store/map.dart';
import 'package:temis/utils/locale/app_localization.dart';

class DetailLocationStoreScreen extends StatefulWidget {
  final int id;

  const DetailLocationStoreScreen({Key key, this.id}) : super(key: key);
  @override
  _DetailLocationStoreScreenState createState() =>
      _DetailLocationStoreScreenState();
}

class _DetailLocationStoreScreenState extends State<DetailLocationStoreScreen> {
  bool isReadDetail = false;
  @override
  Widget build(BuildContext context) {
    if (itemPlaces[widget.id].idPlace == 12) {
      return Scaffold(
        body: SafeArea(
          top: false,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                _background(),
                _buildContent(),
                _buildDirver(),
                _buildBestSale(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: GestureDetector(
          onTap: () {
            AppNavigator.navigateNavigationFastFood();
          },
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.all(5.0),
                child: Sonar(
                  radius: 40,
                  child: CircleAvatar(
                    backgroundImage: AssetImage(itemPlaces[widget.id].image),
                    radius: 100,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 5.0),
                child: Text(
                  "Ghé thăm cửa hàng",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      );
    } else if (itemPlaces[widget.id].idPlace == 11) {
      return Scaffold(
        body: SafeArea(
          top: false,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                _background(),
                _buildContent(),
                _buildDirver(),
                _buildBestSale(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: GestureDetector(
          onTap: (){
            AppNavigator.navigateNavigationCafe(id: widget.id);
          },
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.all(5.0),
                child: Sonar(
                  radius: 40,
                  child: CircleAvatar(
                    backgroundImage: AssetImage(itemPlaces[widget.id].image),
                    radius: 100,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 5.0),
                child: Text(
                  "Ghé thăm cửa hàng",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        body: SafeArea(
          top: false,
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: [
                _background(),
                _buildContent(),
                _buildDirver(),
                _buildBestSale(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: GestureDetector(
          onTap: null,
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.all(5.0),
                child: Sonar(
                  radius: 40,
                  child: CircleAvatar(
                    backgroundImage: AssetImage(itemPlaces[widget.id].image),
                    radius: 100,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 5.0),
                child: Text(
                  "Ghé thăm cửa hàng",
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }

  Widget _background() {
    return Stack(
      children: [
        Container(
          height: 200,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: new DecorationImage(
              image: new ExactAssetImage(itemPlaces[widget.id].image),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
            top: 25,
            left: 10,
            child: WidgetAppbarMenuBack(
              color: Colors.black,
            )),
        Positioned(
          top: 25,
          right: 15,
          child: Row(
            children: [
              GestureDetector(
                onTap: () {},
                child: Container(
                    height: 25,
                    width: 25,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.grey[100]),
                    //padding: EdgeInsets.all(5),
                    child: Center(
                        child: Icon(
                          Icons.search,
                          color: Colors.black,
                          size: 20,
                        ))),
              ),
              SizedBox(
                width: 20,
              ),
              GestureDetector(
                onTap: () {},
                child: Container(
                  height: 25,
                  width: 25,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.grey[100]),
                  padding: EdgeInsets.all(5),
                  child: Image.asset(
                    "assets/images/ic_more.png",
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _buildContent() {
    return Container(
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        padding: EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              itemPlaces[widget.id].name,
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Row(
              children: [
                Icon(
                  Icons.star,
                  color: Colors.yellow,
                  size: 15,
                ),
                Text(
                  itemPlaces[widget.id].star.toString(),
                  style: TextStyle(fontSize: 14),
                ),
              ],
            ),
            Divider(
              height: 20,
              thickness: 1,
            ),
            GestureDetector(
              onTap: () {
                if(itemPlaces[widget.id].lat != null && itemPlaces[widget.id].long != null){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            PositionStore(id: widget.id)),
                  );
                }
              },
              child: Container(
                height: 50,
                child: Row(
                  children: [
                    Expanded(
                      flex: 6,
                      child: RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(children: <InlineSpan>[
                          TextSpan(
                              text: AppLocalizations.of(context)
                                  .translate('payment.address'),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold)),
                          TextSpan(
                            text: itemPlaces[widget.id].address != null
                                ? itemPlaces[widget.id].address
                                : AppLocalizations.of(context)
                                .translate('intro_null.detail'),
                            style: TextStyle(
                              color: Colors.blueAccent,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ]),
                      ),
                    ),
                    Expanded(
                        flex: 1,
                        child: Container(
                          height: 35,
                          width: 35,
                          decoration: BoxDecoration(shape: BoxShape.circle),
                          child: Icon(
                            Icons.assistant_direction,
                            color: Colors.blue,
                            size: 35,
                          ),
                        )),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Container(
                height: isReadDetail ? null : 50,
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(children: <InlineSpan>[
                    TextSpan(
                        text: AppLocalizations.of(context)
                            .translate('intro.detail'),
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 14,
                            fontWeight: FontWeight.bold)),
                    TextSpan(
                        text: itemPlaces[widget.id].intro != null
                            ? itemPlaces[widget.id].intro
                            : AppLocalizations.of(context)
                            .translate('intro_null.detail'),
                        style: TextStyle(color: Colors.black, fontSize: 14)),
                  ]),
                ),
              ),
            ),
            itemPlaces[widget.id].intro != null &&
                itemPlaces[widget.id].intro.length > 100
                ? Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      isReadDetail = !isReadDetail;
                    });
                  },
                  child: isReadDetail
                      ? Text(
                    "Thu gọn",
                    style: TextStyle(color: Colors.blue),
                  )
                      : Text(
                    "Xem thêm",
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
                SizedBox(
                  width: 20,
                )
              ],
            )
                : SizedBox(),
          ],
        ));
  }

  Widget _buildBestSale() {
    return Container(
        height: 260,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        //margin: EdgeInsets.only(top: 20),
        padding: EdgeInsets.all(15),
        // decoration: BoxDecoration(
        //   image: new DecorationImage(
        //     image: new ExactAssetImage(itemPlaces[widget.id].image),
        //     fit: BoxFit.cover,
        //   ),
        // ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              AppLocalizations.of(context).translate('best_seller.title'),
              style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              textAlign: TextAlign.left,
            ),
            SizedBox(
              height: 10,
            ),
            Container(
                height: 110,
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  scrollDirection: Axis.horizontal,
                  itemCount: itemBestSeller.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => LocationStoreScreen(id: index+1 , title: itemFoodService[index].name,)),
                        // );
                      },
                      child: Card(
                          color: Colors.white,
                          elevation: 2,
                          child: Container(
                            //height: 110,
                            width: MediaQuery.of(context).size.width * 0.8,
                            padding: EdgeInsets.all(0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(5),
                                  child:
                                  Image.asset(itemBestSeller[index].image),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        itemBestSeller[index].name.length < 35
                                            ? itemBestSeller[index].name
                                            : itemBestSeller[index]
                                            .name
                                            .substring(0, 35) +
                                            '...',
                                        style: TextStyle(fontSize: 14),
                                        textAlign: TextAlign.left,
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        itemBestSeller[index].price != null
                                            ? AppValue.APP_MONEY_FORMAT.format(
                                            itemBestSeller[index].price)
                                            : "",
                                        style: TextStyle(
                                            fontSize: 12,
                                            decoration:
                                            TextDecoration.lineThrough,
                                            fontStyle: FontStyle.italic),
                                        textAlign: TextAlign.justify,
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        AppValue.APP_MONEY_FORMAT.format(
                                            itemBestSeller[index].priceSale),
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.justify,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )),
                    );
                  },
                ))
          ],
        ));
  }

  Widget _buildDirver() {
    return Container(
      height: 10,
      color: Colors.grey[300],
    );
  }
}

class ColorSonarDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ColorSonar(
          // wavesDisabled: true,
          // waveMotion: WaveMotion.synced,
          contentAreaRadius: 48.0,
          waveFall: 15.0,
          // waveMotionEffect: Curves.elasticIn,
          waveMotion: WaveMotion.synced,
          // duration: Duration(seconds: 5),
          child: CircleAvatar(
            radius: 48.0,
            backgroundImage: AssetImage('assets/avatars/man.png'),
          ),
        ),
      ),
    );
  }
}
