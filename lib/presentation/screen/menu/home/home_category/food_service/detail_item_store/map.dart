import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:temis/model/entity_offline/places.dart';

class PositionStore extends StatefulWidget {
  final int id;

  const PositionStore({Key key, this.id}) : super(key: key);
  @override
  _PositionStoreState createState() => _PositionStoreState();
}

class _PositionStoreState extends State<PositionStore> {
  GoogleMapController mapController;
  static LatLng _center = const LatLng(20.836648, 106.694363);
  final Set<Marker> _markers = {};
  LatLng _currentMapPosition = _center;


  void _onCameraMove(CameraPosition position) {
    _currentMapPosition = position.target;
  }

  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
  }

  @override

  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GoogleMap(
            initialCameraPosition: CameraPosition(
              target: _center,
              zoom: 10.0,
            ),
            markers: _markers,
            onCameraMove: _onCameraMove,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
              setState(() {
                  final marker = Marker(
                    markerId: MarkerId(itemPlaces[widget.id].name),
                    position: LatLng(itemPlaces[widget.id].lat, itemPlaces[widget.id].long),
                    infoWindow: InfoWindow(
                      title: itemPlaces[widget.id].name,
                      // onTap: () => AppNavigator.navigateNewsDetail(state.post[i].id),
                    ),
                    icon: BitmapDescriptor.defaultMarker,
                  );
                  _markers.add(marker);

              });
            }
        ),

      ],
    );
  }
}
