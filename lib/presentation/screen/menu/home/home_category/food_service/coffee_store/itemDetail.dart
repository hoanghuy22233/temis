import 'package:flutter/material.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/model/entity_offline/coffee_item.dart';
import 'package:temis/presentation/common_widgets/widget_appbar_menu_back.dart';

class ItemDetailScreen extends StatefulWidget {
  final int id;

  const ItemDetailScreen({Key key, this.id}) : super(key: key);
  @override
  _ItemDetailScreenState createState() => _ItemDetailScreenState();
}

class _ItemDetailScreenState extends State<ItemDetailScreen> {
  double total;
  int quantity;

  @override
  initState() {
    super.initState();
    total = coffeeItem[widget.id].price;
    quantity = 1;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              color: Color(0xFFeddada)
          ),
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  ClipPath(
                    clipper: MyClipper(),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      height: 230,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        // gradient: LinearGradient(
                        //   begin: Alignment.topLeft,
                        //   end: Alignment(0.8, 0.0),
                        //   colors: [
                        //     Color(0xFFc7a173),Color(0xFFebc9a2),
                        //   ],
                        //   tileMode: TileMode.repeated,
                        // ),
                        image: new DecorationImage(
                          image: new ExactAssetImage('assets/images/background_coffee.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),

                    ),
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 160),
                      child: Hero(
                        tag: coffeeItem[widget.id].name,
                        child: ClipOval(
                          child: Container(

                            child: new Image.asset(
                              coffeeItem[widget.id].image,
                              fit: BoxFit.cover,
                              width: 125,
                              height: 125,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 0, top: 25),
                    child: WidgetAppbarMenuBack(),
                  )
                ],
              ),
              SizedBox(
                height: 25,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: <Widget>[
                    Text(
                      coffeeItem[widget.id].name,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    coffeeItem[widget.id].star != null ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(coffeeItem[widget.id].star.toString(), style: TextStyle(fontSize: 20),),
                        Icon(Icons.star, size: 15, color: Colors.orangeAccent),
                      ],
                    )  : Container(),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          AppValue.APP_MONEY_FORMAT.format(coffeeItem[widget.id].price),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.black54),
                        ),
                        Text(
                          " / suất",
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: 12,
                              color: Colors.black),
                        )
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[

                        Text(
                          "Số lượng",
                          style: TextStyle(
                              fontWeight: FontWeight.normal, fontSize: 20),
                        ),
                        Container(
                          decoration: new BoxDecoration(
                            color: Color(0xFFDAB68C),
                            borderRadius: new BorderRadius.circular(250),
                          ),
                          width: 120,
                          height: 35,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  decoration: new BoxDecoration(
                                    color: Color(0xffa6662e),
                                    borderRadius: new BorderRadius.circular(250),
                                  ),
                                  child: GestureDetector(
                                    onTap: (){
                                      remove(coffeeItem[widget.id].price);
                                    },
                                    child: Icon(
                                      Icons.remove,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Text(
                                  "$quantity",
                                  style: TextStyle(
                                      //color: Colors.black,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 18),
                                ),
                                Container(
                                  decoration: new BoxDecoration(
                                    color: Color(0xffa6662e),
                                    borderRadius: new BorderRadius.circular(250),
                                  ),
                                  child: GestureDetector(
                                    onTap: (){
                                      add(coffeeItem[widget.id].price);
                                    },
                                    child: Icon(
                                      Icons.add,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[

                        Text(
                          "Tổng tiền",
                          style: TextStyle(
                              fontWeight: FontWeight.normal, fontSize: 20),
                        ),
                        Container(
                          decoration: new BoxDecoration(
                            color: Color(0xFFeddada),
                            border: Border.all(color: const Color(0xffa6662e)),
                            borderRadius: new BorderRadius.circular(25),
                          ),
                          width: 120,
                          height: 35,
                          child: Center(child: Text(AppValue.APP_MONEY_FORMAT.format(total),
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  //fontSize: 20,
                                  color: Color(0xffa6662e))
                            )
                          ),
                        ),

                      ],
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          height: 40,
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            color: Color(0xff260801),
                            child: Text("Đặt ngay",
                                style:
                                    TextStyle(color: Colors.white, fontSize: 22)),
                            onPressed: () {},
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void add(double price) {
    setState(() {
      quantity = quantity + 1;
      total = price * quantity;
    });
  }

  void remove(double price) {
    setState(() {
      if (quantity > 1) {
        quantity = quantity - 1;
        total = price * quantity;
      }
    });
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height / 1.3);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height /1.3);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
