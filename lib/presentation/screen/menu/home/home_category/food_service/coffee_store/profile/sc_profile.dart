import 'package:flutter/material.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/profile/stack_container.dart';


class ProfileAccountScreen extends StatefulWidget {
  final int id;

  const ProfileAccountScreen({Key key, this.id}) : super(key: key);

  @override
  _ProfileAccountScreenState createState() => _ProfileAccountScreenState();
}

class _ProfileAccountScreenState extends State<ProfileAccountScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          StackContainer(id: widget.id,),
          ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            children: <Widget>[
              GestureDetector(
                onTap: (){
                  print("1122");
                },
                  child: _widgetCardItem("Quản lý tài khoản","assets/images/user.png",Colors.black)),
              GestureDetector(
                  onTap: (){
                    print("1122");
                  },
                  child: _widgetCardItem("Lịch sử đơn hàng","assets/images/history.png",Colors.black),),
              GestureDetector(
                  onTap: (){
                    print("1122");
                  },
                  child: _widgetCardItem("Trợ giúp","assets/images/question.png",Colors.black),),
              GestureDetector(
                  onTap: (){
                    print("1122");
                  },
                  child: _widgetCardItem("Cài đặt","assets/images/settings.png",Colors.black),),
              GestureDetector(
                  onTap: (){
                    print("1122");
                  },
                  child: _widgetCardItem("Đăng xuất","assets/images/logout.png",Colors.red),),

            ],
          ),
          SizedBox(height: 10,)
        ],
      ),
    );
  }

  Widget _widgetCardItem(String name, String imageIcon, Color colorIcon){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        decoration: BoxDecoration(
            border: Border.all(color: Color(0xffa14b0a)),
          borderRadius: BorderRadius.circular(15),
          color: Color(0xFFDAB68C)
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 25.0,
            vertical: 10.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 25,
                width: 25,

                child: Image.asset(imageIcon, fit: BoxFit.fill, color: colorIcon,),
              ),
              SizedBox(width: 24.0),
              Text(
                name,
                style: TextStyle(
                  fontSize: 16.0,
                  color: Colors.white
          ),
        ),
            ],
          ),
        ),
      ),
    );
  }
}
