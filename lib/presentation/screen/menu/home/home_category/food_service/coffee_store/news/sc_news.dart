import 'package:flutter/material.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/news/wiget_search_news.dart';

class NewsScreen extends StatefulWidget {
  @override
  NewsScreenState createState() => new NewsScreenState();
}

class NewsScreenState extends State<NewsScreen> {
  double heght;
  int type;
  PageController pageController = PageController();
  Duration _animationDuration = Duration(milliseconds: 500);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Material(
          color: Colors.transparent,
          child: Container(
            color: Color(0xFFeddada),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: PageView(
                    controller: pageController,
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          NestedScrollView(
                              headerSliverBuilder: (BuildContext context,
                                  bool innerBoxIsScrolled) {
                                return <Widget>[
                                  SliverAppBar(
                                    expandedHeight:
                                        MediaQuery.of(context).size.height / 5,
                                    floating: false,
                                    pinned: true,
                                    flexibleSpace: FlexibleSpaceBar(
                                      centerTitle: true,
                                      background: _buildContent(),
                                    ),
                                  ),
                                ];
                              },
                              body: _buildCategory()),
                          Container(
                            color: Color(0xFFeddada),
                            height: MediaQuery.of(context).size.height * 0.1,
                            child: Padding(
                              padding: EdgeInsets.only(top: 0.0, left: 0.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  WidgetAppbarMenuBack(),
                                  Expanded(
                                    child: Hero(
                                      tag: 'search',
                                      child: GestureDetector(
                                        onTap: () {
                                          //AppNavigator.navigateSearch();
                                        },
                                        child: AbsorbPointer(
                                          absorbing: true,
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: AppValue
                                                    .APP_HORIZONTAL_PADDING),
                                            child: WidgetSearchNews(),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
//            ),
//          ),
        ),
      ],
    );
  }

  Widget _buildCategory() {
    return Container(
      height: MediaQuery.of(context).size.height / 4,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(75.0)),
      ),
      child: ListView(
        primary: false,
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(left: 25.0, right: 20.0),
        children: <Widget>[
          Padding(
              padding: EdgeInsets.only(top: 30.0),
              child: Container(
                  height: MediaQuery.of(context).size.height - 100,
                  child: ListView(children: [
                    _buildNewsItem('assets/images/image2.jpg',
                        'Mừng giáng sinh', 'Giảm giá từ ngày 20/12 đến 25/12'),
                    _buildNewsItem('assets/images/image3.jpg',
                        'Mừng giáng sinh', 'Giảm giá từ ngày 20/12 đến 25/12'),
                    _buildNewsItem('assets/images/image4.jpg',
                        'Mừng giáng sinh', 'Giảm giá từ ngày 20/12 đến 25/12'),
                    _buildNewsItem('assets/images/image5.jpg',
                        'Mừng giáng sinh', 'Giảm giá từ ngày 20/12 đến 25/12'),
                    _buildNewsItem('assets/images/image5.jpg',
                        'Mừng giáng sinh', 'Giảm giá từ ngày 20/12 đến 25/12'),
                    _buildNewsItem('assets/images/image4.jpg',
                        'Mừng giáng sinh', 'Giảm giá từ ngày 20/12 đến 25/12'),
                    _buildNewsItem('assets/images/image5.jpg',
                        'Mừng giáng sinh', 'Giảm giá từ ngày 20/12 đến 25/12'),
                    _buildNewsItem('assets/images/image4.jpg',
                        'Mừng giáng sinh', 'Giảm giá từ ngày 20/12 đến 25/12'),
                    _buildNewsItem(
                        'assets/images/image5.jpg', 'Salmon bowl', '\$24.00'),
                    _buildNewsItem(
                        'assets/images/image5.jpg', 'Spring bowl', '\$22.00'),
                    _buildNewsItem(
                        'assets/images/image5.jpg', 'Avocado bowl', '\$26.00'),
                    _buildNewsItem(
                        'assets/images/image5.jpg', 'Berry bowl', '\$24.00'),
                    _buildNewsItem(
                        'assets/images/image5.jpg', 'Salmon bowl', '\$24.00'),
                    _buildNewsItem(
                        'assets/images/image5.jpg', 'Spring bowl', '\$22.00'),
                    _buildNewsItem(
                        'assets/images/image5.jpg', 'Avocado bowl', '\$26.00'),
                    _buildNewsItem(
                        'assets/images/image3.jpg', 'Berry bowl', '\$24.00')
                  ]))),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Container(
      color: Color(0xFFeddada),
      padding: EdgeInsets.only(top: 80),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 50.0),
            child: Row(
              children: <Widget>[
                Text('Tin tức cửa hàng',
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 25.0)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNewsItem(String imgPath, String foodName, String price) {
    return Padding(
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
        child: InkWell(
            onTap: () {
              // Navigator.of(context).push(MaterialPageRoute(
              //     builder: (context) => DetailsPage(heroTag: imgPath, foodName: foodName, foodPrice: price)
              // ));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Hero(
                      tag: imgPath,
                      child: Image(
                          image: AssetImage(imgPath),
                          fit: BoxFit.cover,
                          height: 75.0,
                          width: 75.0)),
                ),
                SizedBox(width: 15.0),
                Expanded(
                  flex: 3,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(foodName,
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold)),
                        Container(
                          child: Text(
                            price,
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontSize: 14.0,
                                color: Colors.grey),
                            textAlign: TextAlign.justify,
                          ),
                        )
                      ]),
                )
              ],
            )));
  }
}
