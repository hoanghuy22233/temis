import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:im_animations/im_animations.dart';
import 'package:temis/model/entity_offline/places.dart';
import 'package:temis/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/profile/custom_clipper.dart';
import 'top_bar.dart';

class StackContainer extends StatelessWidget {
  final int id;
  const StackContainer({
    Key key, this.id,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300.0,
      child: Stack(
        children: <Widget>[
          Container(),
          ClipPath(
            clipper: MyCustomClipper(),
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end:
                  Alignment(0.8, 0.0), // 10% of the width, so there are ten blinds.
                  colors: [
                    const Color(0xffd9984e),
                    const Color(0xfff0b789),
                    const Color(0xffedc19d),
                  ], // red to yellow
                  tileMode: TileMode.repeated, // repeats the gradient over the canvas
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment(0, 1),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: AssetImage(itemPlaces[id].image),
                  radius: 80,
                ),
                SizedBox(height: 5.0),
                Text(
                  itemPlaces[id].name,
                  style: TextStyle(
                    fontSize: 21.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black
                  ),
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Text(
                    itemPlaces[id].address,
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Color(0xff803e09),
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 0, right: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                WidgetAppbarMenuBack(),
                GestureDetector(
                  onTap: (){
                    print("Alolo");
                  },
                  child: Sonar(
                    radius: 30,
                    child: Container(
                      height: 25,
                      width: 25,
                      child: Image.asset('assets/images/vip-card.png', color: Color(0xffa10505),),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
