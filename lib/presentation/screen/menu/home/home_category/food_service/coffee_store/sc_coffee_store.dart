import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:bottom_bars/bottom_bars.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/all_product/sc_all_product.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/introduce/sc_introduce.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/itemDetail.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/news/sc_news.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/news/wiget_search_news.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/profile/sc_profile.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/widget_banner_coffee.dart';

class CoffeeStore extends StatefulWidget {
  @override
  _CoffeeStoreState createState() => _CoffeeStoreState();
}

class _CoffeeStoreState extends State<CoffeeStore> {

  int idStore;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        idStore = arguments['idStore'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    return BottomBars(
      drag: true,
      type: 0,
      // floatingActionButton: [
      //   FloatingActionButton(
      //     onPressed: () {},
      //     child: Icon(Icons.backspace),
      //   ),
      //   null,
      //   null,
      //   null,
      // ],
      appBar: [

      ],
      colorItems: Colors.white,
      backgroundColor: Color(0xFF473D3A),
      tabs: <Widget>[
        Center(child: _widgetIntro()),

        Container(
          decoration: BoxDecoration(
              color: Color(0xFFeddada)
          ),
          child: _widgetNews(),
        ),
        Container(
          decoration: BoxDecoration(
              color: Color(0xFFeddada)
          ),
          child:
              _widgetAllProduct(),
        ),
        Container(
          decoration: BoxDecoration(
              color: Color(0xFFeddada)
          ),
           child: _widgetProfile(),
        ),
      ],
      items: [
        BottomBarsItem(
          icon: Icon(Icons.new_releases_outlined),
          title: Text("Giới thiệu"),
          color: Colors.white,
        ),
        BottomBarsItem(
          icon: Icon(Icons.my_library_books_outlined),
          title: Text("Tin tức"),
          color: Colors.white,
        ),
        BottomBarsItem(
            icon: Icon(Icons.restaurant_menu),
            title: Text("Sản phẩm"),
            color: Colors.white),
        BottomBarsItem(
            icon: Icon(Icons.person),
            title: Text("Tài khoản"),
            color: Colors.white),

      ],
    );
  }

  Widget _widgetIntro() => IntroduceScreen();

  Widget _widgetNews() => NewsScreen();

  Widget _widgetAllProduct() => AllProductScreen();

  Widget _widgetProfile() => ProfileAccountScreen(id: idStore);
}