import 'package:flutter/cupertino.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/widget_banner_coffee.dart';

class IntroduceScreen extends StatefulWidget {
  @override
  _IntroduceScreenState createState() => _IntroduceScreenState();
}

class _IntroduceScreenState extends State<IntroduceScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFeddada),
      height: MediaQuery.of(context).size.height-30,
      width: MediaQuery.of(context).size.width,
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return Stack(
            alignment: Alignment.center,
            children: [
              ChildBannerCoffee(
                id: index,
              ),
            ],
          );
        },
        autoplayDelay: 5000,
        autoplay: true,
        itemCount: 5,
        scrollDirection: Axis.horizontal,
        // control: new SwiperControl(),
      ),
    );
  }
}
