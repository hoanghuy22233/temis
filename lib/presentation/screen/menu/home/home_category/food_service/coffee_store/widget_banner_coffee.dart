import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/model/entity_offline/coffee_item.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/coffee_store/itemDetail.dart';

class ChildBannerCoffee extends StatelessWidget {
  final int id;
  ChildBannerCoffee({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          height: 410.0,
          child: ListView(scrollDirection: Axis.horizontal, children: [
            Padding(
                padding: EdgeInsets.only(left: 15.0, right: 15.0),
                child: Container(
                    height: 300.0,
                    width: 225.0,
                    margin: EdgeInsets.only(left: 55.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Stack(alignment: Alignment.center, children: [
                          Container(
                            height: 335.0,
                          ),
                          Positioned(
                              top: 75.0,
                              child: Container(
                                  padding:
                                      EdgeInsets.only(left: 10.0, right: 20.0),
                                  height: 260.0,
                                  width: 225.0,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(25.0),
                                      color: Color(0xFFDAB68C)),
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(
                                          height: 60,
                                        ),
                                        Text(
                                          coffeeItem[id].name,
                                          textAlign: TextAlign.justify,
                                          style: TextStyle(
                                              fontFamily: 'varela',
                                              fontSize: 20.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        SizedBox(height: 40.0),
                                        Text(
                                          AppValue.APP_MONEY_FORMAT
                                              .format(coffeeItem[id].price),
                                          style: TextStyle(
                                              fontFamily: 'varela',
                                              fontSize: 25.0,
                                              fontWeight: FontWeight.bold,
                                              color: Color(0xFF3A4742)),
                                        ),
                                        SizedBox(height: 20.0),
                                        Container(
                                            height: 40.0,
                                            width: 40.0,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(20.0),
                                                color: Colors.white),
                                            child: Center(
                                                child: Icon(Icons.favorite,
                                                    color: Colors.red,
                                                    size: 15.0)))
                                      ]))),
                          Positioned(
                              left: 60.0,
                              top: 25.0,
                              child: Container(
                                  height: 100.0,
                                  width: 100.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(coffeeItem[id].image),
                                        fit: BoxFit.fill),
                                  )))
                        ]),
                        SizedBox(height: 20.0),
                        InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ItemDetailScreen(
                                          id: id,
                                        )),
                              );
                            },
                            child: Container(
                                height: 50.0,
                                width: 225.0,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25.0),
                                    color: Color(0xFF473D3A)),
                                child: Center(
                                    child: Text('Đặt ngay',
                                        style: TextStyle(
                                            fontFamily: 'nunito',
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white)))))
                      ],
                    )))
          ])),
    );
  }
}
