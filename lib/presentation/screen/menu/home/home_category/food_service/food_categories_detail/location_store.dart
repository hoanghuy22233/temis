import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/places.dart';
import 'package:temis/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:temis/presentation/common_widgets/widget_appbar.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/detail_item_store/detail_location_store.dart';

class LocationStoreScreen extends StatefulWidget {
  final int id;
  final String title;

  const LocationStoreScreen({Key key, this.id, this.title}) : super(key: key);
  @override
  _LocationStoreScreenState createState() => _LocationStoreScreenState();
}

class _LocationStoreScreenState extends State<LocationStoreScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Container(
          color: Colors.white,
          padding: EdgeInsets.only(top: 20),
          // height: MediaQuery.of(context).size.height * 0.1,
          child: _appbar(),
        ),
        Expanded(child: _buildListMenu())
      ],
    ));
  }

  Widget _appbar() => WidgetAppbar(
        left: [WidgetAppbarMenuBack()],
        title: widget.title,
      );

  Widget _buildListMenu() {
    return Container(
        //height: MediaQuery.of(context).size.height / 3,
        child: ListView.builder(
      padding: EdgeInsets.zero,
      scrollDirection: Axis.vertical,
      itemCount: itemPlaces.length,
      itemBuilder: (context, index) {
        return itemPlaces[index].idFoodService == widget.id
            ? GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            DetailLocationStoreScreen(id: index)),
                  );
                },
                child: Card(
                    color: Colors.white,
                    elevation: 1,
                    child: Container(
                      margin: EdgeInsets.all(5),
                      height: 130,
                      width: MediaQuery.of(context).size.width,
                      //padding: EdgeInsets.all(5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                              flex: 2,
                              child: Container(
                                height: 130,
                                width: 80,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  image: new DecorationImage(
                                    image: new ExactAssetImage(
                                        itemPlaces[index].image),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )),
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                              flex: 3,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    itemPlaces[index].name,
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.star,
                                        color: Colors.yellow,
                                        size: 15,
                                      ),
                                      Text(
                                        itemPlaces[index].star.toString(),
                                        style: TextStyle(fontSize: 14),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Địa chỉ: ${itemPlaces[index].address}",
                                    style: TextStyle(fontSize: 14),
                                  ),
                                ],
                              ))
                        ],
                      ),
                    )),
              )
            : SizedBox(
                height: 0,
              );
      },
    ));
  }
}
