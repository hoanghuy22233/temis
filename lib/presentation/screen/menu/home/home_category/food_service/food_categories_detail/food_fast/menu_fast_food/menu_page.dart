import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/menu_fast_food/fast/all_food/all_food.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/menu_fast_food/fast/widget_fast_food_appbar.dart';

class MenuFastFoodPage extends StatefulWidget {
  @override
  _MenuFastFoodPageState createState() => _MenuFastFoodPageState();
}

class _MenuFastFoodPageState extends State<MenuFastFoodPage>
    with SingleTickerProviderStateMixin {
  int activeMenu = 0;
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 3);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 20),
            color: Colors.white,
            // height: MediaQuery.of(context).size.height * 0.1,
            child: _buildAppbar(),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Text(
              "Danh mục",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          _buildTabBarMenu(),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
            child: TabBarView(
              controller: _tabController,
              children: [FoodAll(), FoodAll(), FoodAll()],
            ),
          )),
        ],
      ),
    );
  }

  Widget _buildTabBarMenu() {
    return new Container(
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: "Tất cả món",
          ),
          Tab(
            text: "Món yêu thích",
          ),
          Tab(
            text: "Món mới nhất",
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        indicator: new BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Colors.blue[100],
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 500,
        ),
      ),
    );
  }

  _buildAppbar() => WidgetFastFoodAppbar();
}
