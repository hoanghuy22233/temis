import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/list_fast_food_item.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/menu_fast_food/fast/fast_food_item.dart';

class WidgetListFastGrid extends StatelessWidget {
  final Function cancelBackToHome;
  WidgetListFastGrid({this.cancelBackToHome});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 10, left: 5, right: 5),
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: WidgetFastFoodItem(
              id: index,
            ),
          );
        },
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            // crossAxisSpacing: 10,
            // mainAxisSpacing: 5,
            childAspectRatio: 0.7),
        physics: NeverScrollableScrollPhysics(),
        itemCount: listFastFood.length);
  }
}
