import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:temis/model/entity_offline/list_fast_food_item.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/menu_fast_food/fast/all_food/widget_list_fast_food_hoz_main.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/menu_fast_food/fast/all_food/widget_listview_hoz.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/menu_fast_food/fast/widget_list_fast_food_grid.dart';

class FoodAll extends StatefulWidget {
  final int id;
  FoodAll({Key key, this.id}) : super(key: key);

  @override
  _FoodAllState createState() => _FoodAllState();
}

class _FoodAllState extends State<FoodAll> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 4,
            child: Swiper(
              itemBuilder: (BuildContext context, int index) {
                return WidgetFastFoodHozItem(
                  id: index,
                );
              },
              pagination: new SwiperPagination(
                builder: new DotSwiperPaginationBuilder(
                    color: Colors.grey, activeColor: Colors.white),
              ),
              autoplay: true,
              itemCount: listFastFood.length,
              scrollDirection: Axis.horizontal,
              // control: new SwiperControl(),
            ),
          ),
          Container(
            child: _buildListNew(),
          )
        ],
      ),
    );
  }

  _buildListNew() => WidgetListFastGrid();
  _buildListFood() => WidgetListFastFoodMain();
}
