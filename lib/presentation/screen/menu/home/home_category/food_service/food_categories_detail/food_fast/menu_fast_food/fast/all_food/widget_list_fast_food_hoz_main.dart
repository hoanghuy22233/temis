import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/list_fast_food_item.dart';
import 'package:temis/presentation/common_widgets/widget_spacer.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/menu_fast_food/fast/all_food/widget_listview_hoz.dart';

class WidgetListFastFoodMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView.separated(
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return WidgetFastFoodHozItem(
          id: index,
        );
      },
      itemCount: listFastFood.length,
      separatorBuilder: (context, index) {
        return WidgetSpacer(height: 8);
      },
      physics: BouncingScrollPhysics(),
    ));
  }
}
