import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/widget_appbar_food_fast.dart';

class WidgetFastFoodAppbar extends StatelessWidget {
  final String titleText;

  const WidgetFastFoodAppbar({Key key, this.titleText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarFoodFast(
        title: "Món ăn",
        right: [
          GestureDetector(
            onTap: null,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Icon(
                Icons.search,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 20),
            child: GestureDetector(
              child: Badge(
                badgeContent: Text(
                  '3',
                  style: TextStyle(
                    color: Colors.green,
                  ),
                ),
                badgeColor: Colors.white,
                child: Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.circular(15.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          offset: Offset(-3.0, -3.0),
                          blurRadius: 6.0,
                        ),
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          offset: Offset(3.0, 3.0),
                          blurRadius: 6.0,
                        ),
                      ]),
                  child: Icon(
                    Icons.shopping_cart,
                    size: 20.0,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
