import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/presentation/common_widgets/widget_fab_bottom_nav_fast_food.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/home_account/prrofile_food_fast_screen.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/home_food/home_food.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/menu_fast_food/menu_page.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/new_food_fast/new_food_fast.dart';

class TabNavigatorRoutes {
//  static const String root = '/';
  static const String home = '/home';
  static const String news = '/news';
  static const String category = '/category';
  static const String account = '/account';
}

class NavigationFastScreen extends StatefulWidget {
  @override
  _NavigationFastScreenState createState() => _NavigationFastScreenState();
}

class _NavigationFastScreenState extends State<NavigationFastScreen> {
  PageController _pageController;

  List<FABBottomNavFastFoodItem> _navMenus = List();
  int _selectedIndex = 0;

  List<FABBottomNavFastFoodItem> _getTab() {
    return List.from([
      FABBottomNavFastFoodItem.asset(
          route: TabNavigatorRoutes.home,
          tabItem: TabItemFastFood.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/homes.png',
          text: "Trang chủ"),
      FABBottomNavFastFoodItem.asset(
          route: TabNavigatorRoutes.category,
          tabItem: TabItemFastFood.category,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/nav_news.png',
          text: "Menu"),
      FABBottomNavFastFoodItem.asset(
          route: TabNavigatorRoutes.news,
          tabItem: TabItemFastFood.news,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/new.png',
          text: "Tin tức"),
      FABBottomNavFastFoodItem.asset(
          route: TabNavigatorRoutes.account,
          tabItem: TabItemFastFood.account,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/account.png',
          text: "Tài khoản"),
    ]);
  }

  goToPage({int page}) {
    if (page != _selectedIndex) {
      setState(() {
        this._selectedIndex = page;
      });
      _pageController.jumpToPage(_selectedIndex);
    }
  }

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: WidgetFABBottomNavFastFood(
          notchedShape: CircularNotchedRectangle(),
          backgroundColor: Colors.white,
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          items: _navMenus,
          selectedColor: Colors.green,
          color: AppColor.NAV_ITEM_COLOR,
        ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (newPage) {
            setState(() {
              this._selectedIndex = newPage;
            });
          },
          children: [
            HomeFoodFastScreen(),
            MenuFastFoodPage(),
            NewFoodFastCreen(),
            ProfileFootFastScreen(),
          ],
        ));
  }
}
