import 'package:flutter/material.dart';
import 'package:temis/presentation/screen/menu/account/widget_account_appbar.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/home_account/widget_user_item.dart';

class ProfileFootFastScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            color: Colors.green[300],
            padding: EdgeInsets.only(top: 20),
            // height: MediaQuery.of(context).size.height * 0.1,
            child: _buildAppbar(),
          ),
          Expanded(child: WidgetUserItem())
        ],
      ),
    );
  }

  _buildAppbar() => WidgetAccountAppbar();
}
