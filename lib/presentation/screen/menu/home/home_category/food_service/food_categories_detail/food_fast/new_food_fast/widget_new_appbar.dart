import 'package:flutter/material.dart';

import '../widget_appbar_food_fast.dart';

class WidgetNewFoodAppbar extends StatelessWidget {
  final String titleText;

  const WidgetNewFoodAppbar({Key key, this.titleText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarFoodFast(
        title: "Tin tức",
      ),
    );
  }
}
