import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/list_fast_food_item.dart';
import 'package:temis/presentation/common_widgets/widget_spacer.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/new_food_fast/list_new_food_fasst.dart';

class WidgetListNewsFoodFasts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView.separated(
      scrollDirection: Axis.vertical,
      itemBuilder: (context, index) {
        return WidgetListNewsFoodFast(
          id: index,
        );
      },
      itemCount: listFastFood.length,
      separatorBuilder: (context, index) {
        return WidgetSpacer(height: 8);
      },
      physics: BouncingScrollPhysics(),
    ));
  }
}
