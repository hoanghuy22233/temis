import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/presentation/screen/menu/account/components/info.dart';
import 'package:temis/presentation/screen/menu/account/components/profile_menu_item.dart';

class WidgetUserItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: <Widget>[
          Info(
            image: "assets/images/logo_food.jpg",
            name: "Bánh tráng trộn Hải Phòng",
            email: "temisvietnam@gmail.com",
          ),
          SizedBox(height: 10), //20
          ProfileMenuItem(
            iconSrc: "assets/icons/historys.png",
            title: "Lịch sử mua hàng",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/icons/newspaper.png",
            title: "Sổ địa chỉ",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/design.png",
            title: "Đăng sản phẩm",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/manager.png",
            title: "Quản lý sản phẩm",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/img_heart_actived.png",
            title: "Yêu thích",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/icons/phone.png",
            title: "Liên hệ",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/connection.png",
            title: "Điều khoản và chính sách",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/images/help.png",
            title: "Trợ giúp",
            press: () {},
          ),
          ProfileMenuItem(
            iconSrc: "assets/icons/exit.png",
            title: "Trở về màn chính chính",
            press: () {
              AppNavigator.navigateNavigation();
            },
          ),
        ],
      ),
    );
  }
}
