import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/list_fast_food_item.dart';

class WidgetFastFoodMenuItem extends StatefulWidget {
  final Function cancelBackToHome;
  final int id;
  WidgetFastFoodMenuItem({Key key, this.id, this.cancelBackToHome})
      : super(key: key);

  @override
  _WidgetFastFoodMenuItemState createState() => _WidgetFastFoodMenuItemState();
}

class _WidgetFastFoodMenuItemState extends State<WidgetFastFoodMenuItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        height: MediaQuery.of(context).size.height / 2,
        width: MediaQuery.of(context).size.width / 2,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            image: DecorationImage(
                image: AssetImage(
                  listFastFood[widget.id].image,
                ),
                fit: BoxFit.cover)),
        child: Center(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  listFastFood[widget.id].name,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(listFastFood[widget.id].salePrice,
                        style: TextStyle(
                            color: Colors.yellow,
                            fontSize: 14,
                            fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      listFastFood[widget.id].price,
                      style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                          decoration: TextDecoration.lineThrough),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
