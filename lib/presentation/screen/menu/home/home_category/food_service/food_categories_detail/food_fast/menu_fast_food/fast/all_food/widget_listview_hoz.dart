import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/list_fast_food_item.dart';

class WidgetFastFoodHozItem extends StatefulWidget {
  final Function cancelBackToHome;
  final int id;
  WidgetFastFoodHozItem({Key key, this.id, this.cancelBackToHome})
      : super(key: key);

  @override
  _WidgetFastFoodHozItemState createState() => _WidgetFastFoodHozItemState();
}

class _WidgetFastFoodHozItemState extends State<WidgetFastFoodHozItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
              listFastFood[widget.id].image,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Column(
              children: [
                Text(
                  listFastFood[widget.id].name,
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 14),
                ),
                Text(
                  listFastFood[widget.id].salePrice,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
