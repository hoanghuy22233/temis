import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/new_food_fast/widget_list_news_food_fast_main.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/food_fast/new_food_fast/widget_new_appbar.dart';

class NewFoodFastCreen extends StatefulWidget {
  final int id;
  NewFoodFastCreen({Key key, this.id}) : super(key: key);

  @override
  _NewFoodFastCreenState createState() => _NewFoodFastCreenState();
}

class _NewFoodFastCreenState extends State<NewFoodFastCreen>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 20),
            color: Colors.white,
            // height: MediaQuery.of(context).size.height * 0.1,
            child: _buildAppbar(),
          ),
          Expanded(child: _buildListNew())
        ],
      ),
    );
  }

  _buildListNew() => WidgetListNewsFoodFasts();
  _buildAppbar() => WidgetNewFoodAppbar();
}
