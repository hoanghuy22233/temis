import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/list_fast_food_item.dart';

class WidgetFastFoodItem extends StatefulWidget {
  final Function cancelBackToHome;
  final int id;
  WidgetFastFoodItem({Key key, this.id, this.cancelBackToHome})
      : super(key: key);

  @override
  _WidgetFastFoodItemState createState() => _WidgetFastFoodItemState();
}

class _WidgetFastFoodItemState extends State<WidgetFastFoodItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (_) => ProductDetailPage(
        //               images: food_data_section_two[index]['img_mul'],
        //               name: food_data_section_two[index]['name'],
        //               price: food_data_section_two[index]['price'],
        //               description: food_data_section_two[index]
        //                   ['description'],
        //             )));
      },
      child: Container(
        decoration: BoxDecoration(),
        width: (MediaQuery.of(context).size.width - 110) / 2,
        height: 250,
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 3,
              top: 3,
              child: Container(
                width: (MediaQuery.of(context).size.width - 110) / 2,
                height: 235,
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(60),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        listFastFood[widget.id].name,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      // Text(
                      //   listFastFood[widget.id].content,
                      //   style: TextStyle(
                      //     fontSize: 12,
                      //   ),
                      // ),
                      // SizedBox(
                      //   height: 6,
                      // ),
                      Text(
                        listFastFood[widget.id].salePrice,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        listFastFood[widget.id].price,
                        style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey,
                            decoration: TextDecoration.lineThrough),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Container(
              width: 130,
              height: 130,
              decoration: BoxDecoration(
                // color: Colors.redAccent,
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage(listFastFood[widget.id].image),
                    fit: BoxFit.cover),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 10)
                ],
              ),
            ),
            Positioned(
                left: 65,
                top: 220,
                child: Icon(
                  Icons.add_circle,
                  size: 28,
                ))
          ],
        ),
      ),
    );
  }
}
