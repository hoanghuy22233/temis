import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/model/entity_offline/food_service.dart';
import 'package:temis/presentation/common_widgets/widget_appbar_menu.dart';
import 'package:temis/presentation/common_widgets/widget_search.dart';
import 'package:temis/presentation/screen/menu/home/home_category/food_service/food_categories_detail/location_store.dart';

class CategoryFood extends StatefulWidget {
  final Function cancelBackToHome;
  final int type;
  CategoryFood({this.cancelBackToHome, this.type});

  @override
  _CategoryFood createState() => _CategoryFood();
}

class _CategoryFood extends State<CategoryFood> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height * 0.1,
            child: _buildNavigationBar(),
          ),
          Expanded(child: _buildListMenu())
        ],
      ),
    );
  }

  _buildNavigationBar() {
    return SafeArea(
      child: Row(
        children: <Widget>[
          Container(
              margin:
                  const EdgeInsets.only(left: AppValue.ACTION_BAR_HEIGHT * 0.2),
              height: 25,
              width: 25,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.grey[100]),
              child: WidgetAppbarMenu(
                icon: Image.asset(
                  'assets/icons/ic_back.png',
                  color: Colors.black,
                ),
                onTap: widget.cancelBackToHome,
              )),
          Expanded(
            child: Hero(
              tag: 'search',
              child: GestureDetector(
                onTap: () {
                  //AppNavigator.navigateSearch();
                },
                child: AbsorbPointer(
                  absorbing: true,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: AppValue.APP_HORIZONTAL_PADDING),
                    child: WidgetSearch(),
                  ),
                ),
              ),
            ),
          ),
          Container(
            child: Image.asset(
              "assets/icons/headphones.png",
              height: 30,
              width: 30,
              color: AppColor.NAV_ITEM_COLOR,
            ),
          ),
          SizedBox(width: 10)
        ],
      ),
    );
  }

  Widget _buildListMenu() {
    return Container(
        //height: MediaQuery.of(context).size.height / 3,
        child: ListView.builder(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.zero,
      scrollDirection: Axis.vertical,
      itemCount: itemFoodService.length,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => LocationStoreScreen(
                        id: index + 1,
                        title: itemFoodService[index].name,
                      )),
            );
          },
          child: Card(
              color: Colors.white,
              elevation: 3,
              child: Container(
                margin: EdgeInsets.all(5),
                height: 100,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        flex: 1,
                        child: Center(
                          child: Container(
                            height: 50,
                            child: Image.asset(itemFoodService[index].image),
                          ),
                        )),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                        flex: 7,
                        child: Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                itemFoodService[index].name,
                                style: TextStyle(fontSize: 16),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                itemFoodService[index].intro.length <= 100
                                    ? itemFoodService[index].intro
                                    : itemFoodService[index]
                                            .intro
                                            .substring(0, 100) +
                                        '...',
                                style: AppStyle.DEFAULT_SMALL
                                    .copyWith(color: AppColor.BLACK),
                                textAlign: TextAlign.start,
                                maxLines: 2,
                              ),
                            ],
                          ),
                        ))
                  ],
                ),
              )),
        );
      },
    ));
  }
}
