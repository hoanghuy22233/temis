import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';

class WidgetCategiry extends StatelessWidget {
  final Widget image;
  final Widget imageTwo;
  final String text;
  final String textTwo;
  final Function onTap;
  final Function onTapTwo;

  const WidgetCategiry(
      {this.image,
      this.text,
      this.onTap,
      this.imageTwo,
      this.textTwo,
      this.onTapTwo});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width *0.2,
          height: MediaQuery.of(context).size.height * 0.15,
          child: Column(
            children: [
              image != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: GestureDetector(
                        onTap: onTap,
                        child: Container(
                          height: 60,
                          width: 60,
                          child: image,
                        ),
                      ),
                    )
                  : SizedBox(),
              SizedBox(
                height: 5,
              ),
              text != null
                  ? Text(
                      text.length <= 14 ? text : text.substring(0, 14) + '...',
                      style: AppStyle.DEFAULT_SMALL
                          .copyWith(color: AppColor.BLACK),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    )
                  : SizedBox(),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.2,
          height: MediaQuery.of(context).size.height * 0.15,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              imageTwo != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: GestureDetector(
                        onTap: onTapTwo,
                        child: Container(
                          height: 60,
                          width: 60,
                          child: imageTwo,
                        ),
                      ),
                    )
                  : SizedBox(),
              SizedBox(
                height: 5,
              ),
              textTwo != null
                  ? Text(
                      textTwo.length <= 14
                          ? textTwo
                          : textTwo.substring(0, 14) + '...',
                      style: AppStyle.DEFAULT_SMALL
                          .copyWith(color: AppColor.BLACK),
                      textAlign: TextAlign.center,
                      maxLines: 2,
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ],
    );
  }
}
