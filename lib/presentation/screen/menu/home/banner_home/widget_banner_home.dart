import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/banner.dart';

class ChildBannerHome extends StatelessWidget {
  final int id;
  ChildBannerHome({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      itemBanner[id].banner,
       fit: BoxFit.cover,
      height: MediaQuery.of(context).size.height / 4,
      width: MediaQuery.of(context).size.width,
    );
  }
}
