import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/presentation/screen/menu/notifications/comming_notifications/comming.dart';
import 'package:temis/presentation/screen/menu/notifications/widget_notifications_appbar.dart';
import 'package:temis/presentation/screen/menu/notifications/your_notification/your_notifications.dart';

class NotificationsScreen extends StatefulWidget {
  final int id;
  NotificationsScreen({Key key, this.id}) : super(key: key);

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen>
    with SingleTickerProviderStateMixin {
  String textNode = 'Tất cả danh mục chuyên môn';
  final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(top: 20),
            // height: MediaQuery.of(context).size.height * 0.1,
            child: _buildAppbar(),
          ),
          SizedBox(
            height: 20,
          ),
          _buildTabBarMenu(),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
            child: TabBarView(
              controller: _tabController,
              children: [YourNotifications(), Comming()],
            ),
          )),
        ],
      ),
    ));
  }

  _buildAppbar() => WidgetNotificationsAppbar();

  Widget _buildTabBarMenu() {
    return new Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          border: Border.all(
            color: Color(0xff66FFCC),
          ),
          borderRadius: BorderRadius.circular(500)),
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: "Tin của bạn",
          ),
          Tab(
            text: "Thông báo chung",
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.white,
        unselectedLabelColor: AppColor.BLACK,
        indicator: new BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Color(0xff66FFCC),
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 500,
        ),
      ),
    );
  }
}
