import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/model/entity_offline/list_new_item.dart';
import 'package:temis/presentation/common_widgets/widget_spacer.dart';

class WidgetListNotificationItem extends StatelessWidget {
  final int id;
  WidgetListNotificationItem({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      //  onTap: action,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Divider(
              color: AppColor.GREY_LIGHTER_3,
              height: 1,
              thickness: 1,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            color: listNews[id].read == 0
                ? AppColor.GREY_LIGHTER_3
                : AppColor.WHITE,
            width: double.infinity,
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(listNews[id].name.toUpperCase(),
                    style: AppStyle.DEFAULT_MEDIUM_BOLD),
                WidgetSpacer(
                  height: 10,
                ),
                Text(listNews[id].content, style: AppStyle.DEFAULT_MEDIUM),
                WidgetSpacer(
                  height: 10,
                ),
                Text(
                  listNews[id].date,
                  style: AppStyle.DEFAULT_SMALL.copyWith(
                      color: AppColor.GREY, fontStyle: FontStyle.italic),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: Divider(
              color: AppColor.GREY_LIGHTER_3,
              height: 1,
              thickness: 1,
            ),
          ),
        ],
      ),
    );
  }
}
