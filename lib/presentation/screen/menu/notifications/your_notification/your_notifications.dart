import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/presentation/common_widgets/widget_appbar_menu_back.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/presentation/screen/menu/new/list_new/widget_list_news_main.dart';
import 'package:temis/presentation/screen/menu/notifications/your_notification/widget_list_notifications.dart';

class YourNotifications extends StatefulWidget {
  final int id;
  YourNotifications({Key key, this.id}) : super(key: key);

  @override
  _YourNotificationsState createState() => _YourNotificationsState();
}

class _YourNotificationsState extends State<YourNotifications>  with SingleTickerProviderStateMixin {
  String textNode = 'Tất cả danh mục chuyên môn';
  final _numPages = 4;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 5.0),
      height: 8.0,
      width: isActive ? 34.0 : 12.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.blue :Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
  @override

  Widget build(BuildContext context) {
    return   Container(
      child: _buildListNew(),
    );
  }
  _buildListNew() => WidgetListNotifications();

}
