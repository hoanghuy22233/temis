import 'package:flutter/material.dart';
import 'package:temis/presentation/common_widgets/widget_appbar.dart';

class WidgetNotificationsAppbar extends StatelessWidget {
  final String titleText;

  const WidgetNotificationsAppbar({Key key, this.titleText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: "Thông báo",
      ),
    );
  }
}
