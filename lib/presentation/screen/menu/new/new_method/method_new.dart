
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/presentation/screen/menu/new/list_new/widget_list_news_main.dart';

class MethodNew extends StatefulWidget {
  final int id;
  MethodNew ({Key key, this.id}) : super(key: key);

  @override
  _MethodNewState createState() => _MethodNewState();
}

class _MethodNewState extends State<MethodNew >  with SingleTickerProviderStateMixin {
  String textNode = 'Tất cả danh mục chuyên môn';
  final _numPages = 4;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 5.0),
      height: 8.0,
      width: isActive ? 34.0 : 12.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.blue :Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }
  @override

  Widget build(BuildContext context) {
    return   Container(
      child: _buildListNew(),
    );
  }
  _buildListNew() => WidgetListNewsMain ();


}
