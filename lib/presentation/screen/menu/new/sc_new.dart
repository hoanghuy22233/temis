import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/presentation/screen/menu/new/widget_new_appbar.dart';

import 'all_new/all_new.dart';
import 'new_method/method_new.dart';
import 'new_new/new_new.dart';
import 'new_sale/sale_new.dart';

class NewScreen extends StatefulWidget {
  final int id;
  NewScreen({Key key, this.id}) : super(key: key);

  @override
  _NewScreenState createState() => _NewScreenState();
}

class _NewScreenState extends State<NewScreen>
    with SingleTickerProviderStateMixin {
  String textNode = 'Tất cả danh mục chuyên môn';
  final _numPages = 4;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: 20),
            color: Colors.white,
            // height: MediaQuery.of(context).size.height * 0.1,
            child: _buildAppbar(),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Card(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 5),
                height: 30,
                color: Colors.grey[100],
                child: Center(
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Image.asset(
                          "assets/images/img_search.png",
                          width: 20,
                          height: 25,
                        ),
                      ),
                      Text(" "),
                      Container(
                        width: 1,
                        height: 20,
                        color: Colors.grey[300],
                      ),
                      Text("  "),
                      Expanded(
                        child: Text("Tìm kiếm thông tin"),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Text(
              "Danh mục",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          _buildTabBarMenu(),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
            child: TabBarView(
              controller: _tabController,
              children: [NewAll(), NewNews(), NewSale(), MethodNew()],
            ),
          )),
        ],
      ),
    ));
  }

  _buildAppbar() => WidgetNewsAppbar();

  Widget _buildTabBarMenu() {
    return new Container(
      // decoration: BoxDecoration(
      //     border: Border.all(color: AppColor.BLACK),
      //     borderRadius: BorderRadius.circular(500)),
      //  height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: "Tất cả",
          ),
          Tab(
            text: "Tin tức Mới",
          ),
          Tab(
            text: "Ưu đãi",
          ),
          Tab(
            text: "Tin trong ngày",
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        indicator: new BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Colors.blue[100],
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 500,
        ),
      ),
    );
  }
}
