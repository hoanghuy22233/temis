
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/model/entity_offline/list_new_item.dart';
import 'package:temis/presentation/common_widgets/widget_spacer.dart';
import 'package:temis/presentation/screen/menu/new/list_new/list_new.dart';



class WidgetListNewsMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          return WidgetListNews(id: index,);
        },
        itemCount: listNews.length,
        separatorBuilder: (context, index) {
          return WidgetSpacer(height: 8);
        },
        physics: BouncingScrollPhysics(),
      )
    );
  }


}
