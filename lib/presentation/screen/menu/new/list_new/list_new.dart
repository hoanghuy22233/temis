
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:temis/app/constants/color/color.dart';
import 'package:temis/app/constants/style/style.dart';
import 'package:temis/app/constants/value/value.dart';
import 'package:temis/model/entity_offline/list_new_item.dart';
import 'package:temis/presentation/common_widgets/widget_spacer.dart';
import 'package:temis/presentation/screen/detail_new/detail_new_new.dart';

class WidgetListNews extends StatelessWidget {
  final int id;
  WidgetListNews({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => DetailNewNewScreen(id: id)),

      ),
      child: Card(
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Container(
            height: 95,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AspectRatio(
                  aspectRatio: AppValue.NEWS_IMAGE_RATIO,
                  child: Image.asset(listNews[id].image),
                ),
                const SizedBox(width: 4,),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 4),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          listNews[id].name,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: AppStyle.DEFAULT_SMALL
                              .copyWith(color: AppColor.PRIMARY),
                        ),
                        WidgetSpacer(
                          height: 3,
                        ),
                        Expanded(
                          child: Container(
                              child: Text(
                                listNews[id].content.length <= 70
                                    ?listNews[id].content
                                    : listNews[id].content.substring(0, 70) + '...',
                                style: AppStyle.DEFAULT_SMALL
                                    .copyWith(color: AppColor.GREY),
                                textAlign: TextAlign.start,
                              )),
                        ),
                        WidgetSpacer(
                          height: 3,
                        ),
                        Text(
                          listNews[id].date,
                          style: AppStyle.DEFAULT_SMALL.copyWith(
                              color: AppColor.GREY),
                          textAlign: TextAlign.start,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
