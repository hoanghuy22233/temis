import 'package:flutter/material.dart';
import 'package:temis/presentation/common_widgets/widget_appbar.dart';

class WidgetNewsAppbar extends StatelessWidget {
  final String titleText;

  const WidgetNewsAppbar({Key key, this.titleText}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: "Tin tức",
      ),
    );
  }
}
