import 'dart:ui';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tindercard/flutter_tindercard.dart';
import 'package:temis/app/constants/barrel_constants.dart';
import 'package:temis/presentation/animation/ani.dart';
import 'package:temis/presentation/screen/slide_show/widget_button_post.dart';

class MySlideShowPage extends StatefulWidget {
  MySlideShowPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MySlideShowPageState createState() => _MySlideShowPageState();
}

class _MySlideShowPageState extends State<MySlideShowPage> {
  List<Widget> list = new List();

  @override
  void initState() {
    list.add(GwentCard(
      name: "Xe cộ",
      description:
          "\"Tham gia để biết nhiều thông tin về phương tiện xe cộ, trực tiếp từ các nhà sản xuất hàng đầu.\"",
      power: "12",
      image_asset: "assets/icons/motor.jpg",
      alignment: Alignment(-3.0, -1.0),
    ));

    list.add(GwentCard(
      name: "Việc làm",
      description:
          "\"Tham gia để biết nhiều thông tin về việc làm trong ngày, trực tiếp từ các nhà tuyển dụng hàng đầu.\"",
      power: "15",
      image_asset: "assets/icons/work.png",
      alignment: Alignment(-0.0, -1.0),
    ));

    list.add(GwentCard(
      name: "Du lịch",
      description:
          "\"Tham gia để biết nhiều thông tin về các dịch vụ tốt nhất!\"",
      power: "8",
      image_asset: "assets/icons/travel.jpg",
      alignment: Alignment(-0.0, -1.0),
    ));

    list.add(GwentCard(
      name: "Thú cưng",
      description:
          "\"Tham gia để biết nhiều thông tin về các dịch vụ thú cưng tốt nhất!\"",
      power: "8",
      image_asset: "assets/icons/pet-hello.jpg",
      alignment: Alignment(-0.0, -1.0),
    ));
    list.add(GwentCard(
      name: "Dịch vụ ăn uống",
      description:
          "\"Tham gia để biết nhiều thông tin về các dịch vụ ăn uống tốt nhất!\"",
      power: "8",
      image_asset: "assets/images/food.jpg",
      alignment: Alignment(-0.0, -1.0),
    ));
    list.add(GwentCard(
      name: "Thời trang",
      description:
          "\"Tham gia để biết nhiều thông tin về các dịch vụ thời trang tốt nhất!\"",
      power: "8",
      image_asset: "assets/icons/fashion.jpg",
      alignment: Alignment(-0.0, -1.0),
    ));
    list.add(GwentCard(
      name: "Giáo dục",
      description:
          "\"Tham gia để biết nhiều thông tin về các vấn đề giáo dục tốt nhất!\"",
      power: "8",
      image_asset: "assets/icons/education.jpg",
      alignment: Alignment(-0.0, -1.0),
    ));
    list.add(GwentCard(
      name: "Bất động sản",
      description:
          "\"Tham gia để biết nhiều thông tin về các vấn đề bất động sản tốt nhất!\"",
      power: "8",
      image_asset: "assets/icons/furniture.jpeg",
      alignment: Alignment(-0.0, -1.0),
    ));
    list.add(GwentCard(
      name: "Nội thất,đồ gia dụng...",
      description:
          "\"Tham gia để biết nhiều thông tin về các vấn đề nội thất, đồ gia dụng tốt nhất!\"",
      power: "8",
      image_asset: "assets/icons/electronic.jpg",
      alignment: Alignment(-0.0, -1.0),
    ));
    list.add(GwentCard(
      name: "Giải trí",
      description:
          "\"Tham gia để biết nhiều thông tin về các vấn đề giải trí tốt nhất!\"",
      power: "8",
      image_asset: "assets/icons/entertainment.jpg",
      alignment: Alignment(-0.0, -1.0),
    ));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    CardController controller;

    return Scaffold(
        backgroundColor: Colors.blue,
        body: Stack(
//              children: list,
          children: <Widget>[
            Positioned(
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              child: FadeAnimation(
                  1.5,
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 80),
                    child: Center(
                      child: Text(
                        "Chào các bạn đã tham gia YBC Startups",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w800,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )),
//            Image.asset("assets/images/backround_back.jpg",fit: BoxFit.cover,
//            colorBlendMode: BlendMode.darken,),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Container(
                  color: Colors.transparent,
                  height: MediaQuery.of(context).size.height * 0.9,
                  child: new TinderSwapCard(
                      orientation: AmassOrientation.TOP,
                      totalNum: list.length,
                      stackNum: list.length,
                      swipeEdge: 4.0,
                      maxWidth: MediaQuery.of(context).size.width * 0.9,
                      maxHeight: MediaQuery.of(context).size.height * 0.90,
                      minWidth: MediaQuery.of(context).size.width * 0.8,
                      minHeight: MediaQuery.of(context).size.height * 0.8,
                      cardBuilder: (context, index) => Card(
                            child: list[index],
                          ),
                      cardController: controller = CardController(),
                      swipeUpdateCallback:
                          (DragUpdateDetails details, Alignment align) {
                        /// Get swiping card's alignment
                        if (align.x < 0) {
                          //Card is LEFT swiping
                        } else if (align.x > 0) {
                          //Card is RIGHT swiping
                        }
                      },
                      swipeCompleteCallback:
                          (CardSwipeOrientation orientation, int index) {
                        /// Get orientation & index of swiped card!
                      })),
            ),
            Container(
              margin: EdgeInsets.only(top: 600),
              child: Center(
                child: Container(
                  width: 200,
                  height: AppValue.ACTION_BAR_HEIGHT,
                  child: GestureDetector(
                    onTap: () {
                      AppNavigator.navigateNavigation();
                    },
                    child: Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      color: Colors.white,
                      child: Center(
                          child: Text(
                        "Tham gia",
                        style: AppStyle.DEFAULT_MEDIUM
                            .copyWith(color: AppColor.BLACK),
                      )),
                    ),
                  ),
                ),
              ),
            ),
//              GwentCard(
//                name: "Cirilla Fiona Elen Riannon",
//                description:
//                    "\"Know when fairy tales cease to be tales? When people start believing in them.\"",
//                power: "15",
//                image_asset:  "assets/images/image1.jpg",
//              ),
          ],
        ));
  }

  _buildSubmitButton() {
    return WidgetButtonPost(
      onTap: () {
        AppNavigator.navigateNavigation();
      },
      text: "Tham gia",
    );
  }
}

class GwentCard extends StatelessWidget {
  final String name;
  final String description;
  final String power;
  final String image_asset;
  final Alignment alignment;

  GwentCard(
      {this.name,
      this.description,
      this.power,
      this.image_asset,
      this.alignment});

  List<Color> _colors = [Color(0xFF000000), Color(0xFF000000)];
  List<double> _stops = [0.0, 1.2];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0.0),
            child: Container(
              color: Colors.black,
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 3,
                      child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10)),
                          child: Stack(
                            children: <Widget>[
                              Image.asset(
                                image_asset,
                                width: double.infinity,
                                filterQuality: FilterQuality.low,
                                height:
                                    MediaQuery.of(context).size.height * 0.60 +
                                        4,
                                fit: BoxFit.cover,
                                alignment: alignment,
                              ),
                            ],
                          )),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10)),
                          child: Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                                //border: Border( top:BorderSide(color: Color(0xFF000000),width: 1)),
                                image: DecorationImage(
                                    image: AssetImage(
                                      'assets/images/background.jpg',
                                    ),
                                    alignment: Alignment(0, -1.0),
                                    colorFilter: ColorFilter.mode(
                                        Color(0xffDAD3C7), BlendMode.colorBurn),
                                    fit: BoxFit.fitWidth)),
                            child: Column(
                              children: <Widget>[
                                Flexible(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 20.0),
                                    child: Text(
                                      name,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: "Gwent",
                                          fontSize: 20.0,
                                          color: Color(0xff4E493E)),
                                    ),
                                  ),
                                ),
                                Flexible(
                                  flex: 1,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 12.0),
                                    child: Text(
                                      description,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          //  fontFamily: "Gwent",
                                          fontSize: 15.0,
                                          color: Color(0xff4E493E)),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                    )
                  ],
                ),
              ),
            ),
          ),
          // Positioned(
          //   left: 0,
          //   top: 0,
          //   child: Image.asset("assets/images/bowl.png"),
          // ),
          Positioned(
            left: 37,
            top: MediaQuery.of(context).size.height * 0.05,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: RadialGradient(
                  colors: [
                    Color(0xffffffff),
                    Color(0xffffffff),
                  ],
                  stops: [0.2, 1.2],
                ),
                //border: Border.all(color: Color(0xFF000000),width: 2,style:  BorderStyle.none),
              ),
              child: DottedBorder(
                borderType: BorderType.Circle,
                color: Color(0xffC88E66),
                strokeWidth: 1,
                child: CircleAvatar(
                  radius: 30,
                  backgroundColor: Colors.transparent,
                  child: Image.asset(
                    "assets/images/logo_ybc.png",
                    height: 35,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
